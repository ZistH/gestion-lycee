/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import 'bootstrap-table/dist/bootstrap-table.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/baseStyle.css'

// js import
import 'jquery/dist/jquery.min';
import 'bootstrap/dist/js/bootstrap.min'
import 'bootstrap-table/dist/bootstrap-table.min'
import 'bootstrap-table/dist/locale/bootstrap-table-fr-FR.min'