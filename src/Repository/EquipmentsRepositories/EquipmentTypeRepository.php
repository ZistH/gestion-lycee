<?php

namespace App\Repository\EquipmentsRepositories;

use App\Entity\Equipments\TypeEquipment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypeEquipment>
 *
 * @method TypeEquipment|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeEquipment|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeEquipment[]    findAll()
 * @method TypeEquipment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipmentTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeEquipment::class);
    }

    public function save(TypeEquipment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TypeEquipment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param $currentPage
     * @param $maxResult
     * @return Paginator
     */
    public function findAllPage($currentPage, $maxResult){
        $queryBuilder = $this->createQueryBuilder('et');

        // Add the first and max result limits
        $query = $queryBuilder->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);


        // Generate the Paginator
        $paginator = new Paginator($query, true);
        return $paginator;
    }

//    /**
//     * @return TypeEquipment[] Returns an array of TypeEquipment objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TypeEquipment
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
