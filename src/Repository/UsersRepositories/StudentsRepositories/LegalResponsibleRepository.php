<?php

namespace App\Repository\UsersRepositories\StudentsRepositories;

use App\Entity\Users\Students\LegalResponsible;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LegalResponsible>
 *
 * @method LegalResponsible|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegalResponsible|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegalResponsible[]    findAll()
 * @method LegalResponsible[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegalResponsibleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegalResponsible::class);
    }

    public function save(LegalResponsible $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LegalResponsible $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllLegalResponsible(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_LEGAL_RESPONSIBLE" . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param $currentPage
     * @param $maxResult
     * @return Paginator
     */
    public function findAllPage($currentPage, $maxResult){
        $queryBuilder = $this->createQueryBuilder('user');

        // Add the first and max result limits
        $query = $queryBuilder->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);


        // Generate the Paginator
        $paginator = new Paginator($query, true);
        return $paginator;
    }

//    /**
//     * @return LegalResponsible[] Returns an array of LegalResponsible objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?LegalResponsible
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
