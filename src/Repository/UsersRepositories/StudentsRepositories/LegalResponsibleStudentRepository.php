<?php

namespace App\Repository\UsersRepositories\StudentsRepositories;

use App\Entity\Users\Students\LegalResponsibleStudent;
use App\Entity\Users\Students\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LegalResponsibleStudent>
 *
 * @method LegalResponsibleStudent|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegalResponsibleStudent|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegalResponsibleStudent[]    findAll()
 * @method LegalResponsibleStudent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegalResponsibleStudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegalResponsibleStudent::class);
    }

    public function save(LegalResponsibleStudent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LegalResponsibleStudent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function countLegalResponsibleFromStudent(Student $student) : int
    {
        return count($this->createQueryBuilder('lrs')
            ->where("lrs.student = {$student->getId()}")
            ->getQuery()
            ->getArrayResult());
    }

//    /**
//     * @return LegalResponsibleStudent[] Returns an array of LegalResponsibleStudent objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?LegalResponsibleStudent
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
