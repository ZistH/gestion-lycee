<?php

namespace App\Repository\UsersRepositories\StudentsRepositories;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Users\Students\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Student>
 *
 * @method Student|null find($id, $lockMode = null, $lockVersion = null)
 * @method Student|null findOneBy(array $criteria, array $orderBy = null)
 * @method Student[]    findAll()
 * @method Student[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Student::class);
    }

    public function save(Student $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Student $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param $currentPage
     * @param $maxResult
     * @return Paginator
     */
    public function findAllWithPages($currentPage, $maxResult){
        $queryBuilder = $this->createQueryBuilder('user');

        // Add the first and max result limits
        $query = $queryBuilder->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);

        // Generate the Paginator
        return new Paginator($query, true);
    }


    public function findBySchoolCLassAndYear(SchoolClass $schoolClass, int $year) : array
    {
        $classId = $schoolClass->getId();
        return $this->createQueryBuilder('student')
            ->leftJoin('student.registrations', 're')
            ->where("re.year = $year")
            ->addSelect('re')
            ->leftJoin("re.schoolClass", 'class')
            ->andWhere("class.id = $classId")
            ->addSelect('class')
//            ->leftJoin("class.contract", 'c')
//            ->andWhere("c.date = $year")
            ->getQuery()
            ->getArrayResult();
    }

    public function findAllByYear() : array
    {
        return $this->createQueryBuilder('student')
            ->innerJoin('student.registrations', 'reg')
            ->addSelect('reg')
            ->orderBy('reg.year', 'asc')
            ->getQuery()
            ->getArrayResult();
    }



//    /**
//     * @return Student[] Returns an array of Student objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Student
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
