<?php

namespace App\Repository\UsersRepositories\StaffRepositories;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Users\Staff\Teacher;
use App\Entity\Users\Staff\TeachingContract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TeachingContract>
 *
 * @method TeachingContract|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeachingContract|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeachingContract[]    findAll()
 * @method TeachingContract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeachingContractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TeachingContract::class);
    }

    public function save(TeachingContract $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TeachingContract $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByTeacherAndYear(Teacher $teacher, int $date): TeachingContract|null
    {
        $id = $teacher->getId();

        return $this->createQueryBuilder("tc")
            ->where("tc.teacher = $id")
            ->andWhere("tc.date = $date")
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findBySchoolCLassAndYear(SchoolClass $schoolClass, int $year) : array
    {
        $classId = $schoolClass->getId();

        return $this->createQueryBuilder('tc')
            ->where("tc.date = $year")
            ->leftJoin("tc.schoolClasses", 'class')
            ->where("class.id = $classId")
            ->addSelect('class')
            ->getQuery()
            ->getResult();
    }

//    public function findBySchoolClassAndYear(SchoolClass $schoolClass, int $year) : array
//    {
//        return $this->createQueryBuilder('tc')
//            ->where("tc.date = '$year'")
////            ->leftJoin('tc.schoolClasses', 'schoolClasses')
////            ->where("schoolClasses.contract = tc.id")
//            ->getQuery()
//            ->getArrayResult();
//    }

//    /**
//     * @return TeachingContract[] Returns an array of TeachingContract objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TeachingContract
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
