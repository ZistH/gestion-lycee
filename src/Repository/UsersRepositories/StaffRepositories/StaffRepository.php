<?php

namespace App\Repository\UsersRepositories\StaffRepositories;

use App\Entity\Users\Staff\Staff;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Staff>
 *
 * @method Staff|null find($id, $lockMode = null, $lockVersion = null)
 * @method Staff|null findOneBy(array $criteria, array $orderBy = null)
 * @method Staff[]    findAll()
 * @method Staff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StaffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Staff::class);
    }

    public function save(Staff $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Staff $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findRH(): Staff|null
    {
        return $this->createQueryBuilder('user')
            ->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_RH" . '%')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllStaff(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_STAFF" . '%')
            ->getQuery()
            ->getResult();
    }

    public function findAllScolarite(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_SCOLARITE" . '%')
            ->getQuery()
            ->getResult();
    }


    public function findAllDocumentation(): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_DOCUMENTATION" . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param int $currentPage
     * @param int $maxResult
     * @return Paginator
     */
    public function findAllDocumentationPage(int $currentPage, int $maxResult){
        $queryBuilder = $this->createQueryBuilder('user');

        // Add the first and max result limits
        $query = $queryBuilder->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_DOCUMENTATION" . '%')
            ->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);


        // Generate the Paginator
        $paginator = new Paginator($query, true);
        return $paginator;
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param int $currentPage
     * @param int $maxResult
     * @return Paginator
     */
    public function findAllScolaritePage(int $currentPage, int $maxResult){
        $queryBuilder = $this->createQueryBuilder('user');

        // Add the first and max result limits
        $query = $queryBuilder->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_SCOLARITE" . '%')
            ->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);


        // Generate the Paginator
        $paginator = new Paginator($query, true);
        return $paginator;
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param int $currentPage
     * @param int $maxResult
     * @return Paginator
     */
    public function findAllPage(int $currentPage, int $maxResult){
        $queryBuilder = $this->createQueryBuilder('user');

        // Add the first and max result limits
        $query = $queryBuilder->where('user.roles LIKE :role')
            ->setParameter('role', '%' . "ROLE_STAFF" . '%')
            ->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);


        // Generate the Paginator
        $paginator = new Paginator($query, true);
        return $paginator;
    }
//    /**
//     * @return Staff[] Returns an array of Staff objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Staff
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
