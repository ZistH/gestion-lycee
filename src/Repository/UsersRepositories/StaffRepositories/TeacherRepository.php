<?php

namespace App\Repository\UsersRepositories\StaffRepositories;

use App\Entity\Users\Staff\Teacher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Teacher>
 *
 * @method Teacher|null find($id, $lockMode = null, $lockVersion = null)
 * @method Teacher|null findOneBy(array $criteria, array $orderBy = null)
 * @method Teacher[]    findAll()
 * @method Teacher[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Teacher::class);
    }

    public function save(Teacher $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Teacher $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    /**
     * Retrieve the list of active orders with all their actives packages
     * @param $currentPage
     * @param $maxResult
     * @return Paginator
     */
    public function findAllPage($currentPage, $maxResult){
        $queryBuilder = $this->createQueryBuilder('t');

        // Add the first and max result limits
        $query = $queryBuilder->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult)
        ->getQuery();


        // Generate the Paginator
        return new Paginator($query, true);
    }

    public function findById(int $id): Teacher|null
    {
        return $this->createQueryBuilder("t")
            ->where("t.id = '$id'")
            ->getQuery()
            ->getOneOrNullResult();
    }

//    /**
//     * @return Teacher[] Returns an array of Teacher objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Teacher
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
