<?php

namespace App\Repository\SubjectsRepositories;

use App\Entity\Subjects\IsTaught;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IsTaught>
 *
 * @method IsTaught|null find($id, $lockMode = null, $lockVersion = null)
 * @method IsTaught|null findOneBy(array $criteria, array $orderBy = null)
 * @method IsTaught[]    findAll()
 * @method IsTaught[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IsTaughtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IsTaught::class);
    }

    public function save(IsTaught $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IsTaught $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Retrieve the list of active orders with all their actives packages
     * @param $currentPage
     * @param $maxResult
     * @return Paginator
     */
    public function findAllIsTaughtWithPaging($currentPage, $maxResult) : Paginator
    {
        // Add the first and max result limits
        $query = $this->createQueryBuilder('isTaught')
            ->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);

        // Generate the Paginator
        return new Paginator($query, true);
    }

    public function findAllAreTaughtLanguages(array $languages, $currentPage, $maxResult) : Paginator|null
    {
        $query = $this->createQueryBuilder('it');

        //Ne fonctionne correctement que lorsque $languages n'est pas vide
        foreach ($languages as $language){
            $query->orWhere("it.subject = {$language->getId()}");
        }



        $query->setFirstResult(($currentPage - 1) * $maxResult)
                ->setMaxResults($maxResult);

        return new Paginator($query, true);
    }

    public function findAllAreTaughtForCommonSubject(array $commonSubject, $currentPage, $maxResult) : Paginator
    {
        $query = $this->createQueryBuilder('it');

        //Ne fonctionne correctement que lorsque $commonSubject n'est pas vide
        foreach ($commonSubject as $cs){
            $query->orWhere("it.subject = {$cs->getId()}");
        }

        $query->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);

        return new Paginator($query, true);
    }

    public function findAllAreTaughtForOptionalSubject(array $optinalSubject, $currentPage, $maxResult) : Paginator
    {
        $query = $this->createQueryBuilder('it');

        //Ne fonctionne correctement que lorsque $optinalSubject n'est pas vide
        foreach ($optinalSubject as $os){
            $query->orWhere("it.subject = {$os->getId()}");
        }


        $query->setFirstResult(($currentPage - 1) * $maxResult)
            ->setMaxResults($maxResult);

        return new Paginator($query, true);
    }
//    /**
//     * @return IsTaught[] Returns an array of IsTaught objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?IsTaught
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
