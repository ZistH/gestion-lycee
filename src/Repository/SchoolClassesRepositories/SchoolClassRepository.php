<?php

namespace App\Repository\SchoolClassesRepositories;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\SchoolClasses\SchoolLevel;
use App\Entity\Users\Staff\TeachingContract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SchoolClass>
 *
 * @method SchoolClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolClass[]    findAll()
 * @method SchoolClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SchoolClass::class);
    }

    public function save(SchoolClass $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SchoolClass $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllWithPagination(int $currentPage, int $nbMaxPerPage) : Paginator
    {
        $query = $this->createQueryBuilder('sc')
            ->setFirstResult(($currentPage - 1) * $nbMaxPerPage)
            ->setMaxResults($nbMaxPerPage)
            ->getQuery();

        return new Paginator($query);
    }

    public function findByContractWithPaging(TeachingContract $contract, int $currentPage, int $nbMaxPerPage) : Paginator
    {
        $cid = $contract->getId();

        $query = $this->createQueryBuilder('sc')
            ->leftJoin('sc.contract', 'contract')
            ->where("contract.id = $cid")
            ->setFirstResult(($currentPage - 1) * $nbMaxPerPage)
            ->setMaxResults($nbMaxPerPage)
            ->getQuery();

        return new Paginator($query);
    }

    public function findBySchoolLevelPreUpdate(SchoolLevel $schoolLevel) : array
    {
        return $this->createQueryBuilder('sc')
            ->where("sc.schoolLevel = {$schoolLevel->getId()}")
            ->leftJoin('sc.registrations', 'r')
            ->addSelect('r')
            ->getQuery()
            ->getResult();
    }
    public function findBySchoolLevelPostUpdate(SchoolLevel $schoolLevel) : array
    {
        return $this->createQueryBuilder('sc')
            ->where("sc.schoolLevel = {$schoolLevel->getId()}")
//            ->leftJoin('sc.registrations', 'r')
//            ->addSelect('r')
            ->getQuery()
            ->getResult();
    }

    public function findWithTeachingContractByYearAndId(int $id, int $year) : SchoolClass
    {
        return $this->createQueryBuilder('sc')
            ->leftJoin("sc.contract", 'c')
            ->AndWhere("c.date = $year")
            ->addSelect('c')
            ->AndWhere("sc.id = $id")
            ->getQuery()
            ->getOneOrNullResult();
    }

//    /**
//     * @return SchoolClass[] Returns an array of SchoolClass objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SchoolClass
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
