<?php

namespace App\Entity\Users;

use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\Students\Student;
use App\Entity\Users\Staff\Staff;
use App\Entity\Users\Staff\Teacher;
use App\Repository\UsersRepositories\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\InheritanceType("JOINED")]
#[ORM\DiscriminatorColumn(name: "type", type: "string")]
#[ORM\DiscriminatorMap([
    'student' => Student::class,
    'teacher' => Teacher::class,
    'legalResponsible' => LegalResponsible::class,
    'staff' => Staff::class
])]
#[UniqueEntity('login', message: 'Login déjà utilisé')]
#[UniqueEntity('email', message: 'email déjà utilisé')]
#[ORM\Table(name:'utilisateur')]
abstract class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $login = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $firstname = null;

    #[ORM\Column(length: 255)]
    #[Assert\Email(message: "L'email {{ value }} n'est pas correct")]
    private ?string $email = null;

    #[ORM\Column(length: 15, nullable: true)]
    #[Assert\Regex(pattern: "#(^(0[1-9]\s){1}(\d\d\s){3}(\d{2})$)|(^(0[1-9]){1}\d{8}$)#",
        message: "ce n'est pas un numéro de téléphone valide, il doit commencer par 0 et être composé de 10 chiffres au total ")]
    private ?string $telephone = null; // On autorise l'écriture des numéros sous les formes "xxyyrrttee" et "xx yy rr tt ee"

    #[ORM\Column(type : "boolean")]
    private ?bool $disabled = null;

    #[ORM\ManyToOne(cascade: ["persist"], inversedBy: 'users')]
    #[Assert\Valid]
    private ?Address $address = null;

    #[ORM\Column(length: 255)]
    private ?string $loginPrefix = null;

    #[ORM\Column]
    private ?int $loginSuffix = null;

    public function __construct()
    {
        $this->setDisabled(false);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->login;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLoginPrefix(): ?string
    {
        return $this->loginPrefix;
    }

    public function setLoginPrefix(string $loginPrefix): self
    {
        $this->loginPrefix = $loginPrefix;

        return $this;
    }

    public function getLoginSuffix(): ?int
    {
        return $this->loginSuffix;
    }

    public function setLoginSuffix(int $loginSuffix): self
    {
        $this->loginSuffix = $loginSuffix;

        return $this;
    }
}
