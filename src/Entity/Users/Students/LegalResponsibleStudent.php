<?php

namespace App\Entity\Users\Students;

use App\Repository\UsersRepositories\StudentsRepositories\LegalResponsibleStudentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LegalResponsibleStudentRepository::class)]
#[ORM\UniqueConstraint(columns: ['legalresponsible_id', 'student_id'])]
class LegalResponsibleStudent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'legalResponsibleStudents')]
    private ?Student $student = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'legalResponsibleStudents')]
    private ?LegalResponsible $legalresponsible = null;

    #[ORM\Column]
    private ?int $responsible_number = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getLegalresponsible(): ?LegalResponsible
    {
        return $this->legalresponsible;
    }

    public function setLegalresponsible(?LegalResponsible $legalresponsible): self
    {
        $this->legalresponsible = $legalresponsible;

        return $this;
    }

    public function getResponsibleNumber(): ?int
    {
        return $this->responsible_number;
    }

    public function setResponsibleNumber(int $responsible_number): self
    {
        $this->responsible_number = $responsible_number;

        return $this;
    }
}
