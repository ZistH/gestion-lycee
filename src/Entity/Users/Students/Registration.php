<?php

namespace App\Entity\Users\Students;

use App\Entity\Equipments\Equipment;
use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Subjects\Language;
use App\Entity\Subjects\OptionalSubject;
use App\Repository\UsersRepositories\StudentsRepositories\RegistrationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


#[ORM\Entity(repositoryClass: RegistrationRepository::class)]
class Registration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(name: 'YEAR')]
    private ?int $year;

    #[ORM\ManyToOne(inversedBy: 'registrations')]
    #[Assert\Valid]
    private ?Student $student = null;

    #[ORM\ManyToOne(inversedBy: 'registrations')]
    private ?SchoolClass $schoolClass = null;

//    #[ORM\ManyToMany(targetEntity: Equipment::class, inversedBy: 'registrations')]
//    private Collection $equipment;

    #[ORM\ManyToOne(cascade: ['remove'], inversedBy: 'registration')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Valid]
    private ?Language $language = null;

    #[ORM\ManyToMany(targetEntity: OptionalSubject::class, mappedBy: 'registrations')]
    private Collection $optionalSubjects;

    public function __construct()
    {
        $this->setYear(new \DateTime());
//        $this->equipment = new ArrayCollection();
        $this->optionalSubjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(\DateTime $year): self
    {
        $this->year = date_format($year, "Y");

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;
        $student->setCurrentRegistration($this);

        return $this;
    }

    public function getSchoolClass(): ?SchoolClass
    {
        return $this->schoolClass;
    }

    public function setSchoolClass(?SchoolClass $schoolClass): self
    {
        $this->schoolClass = $schoolClass;

        return $this;
    }

//    /**
//     * @return Collection<int, Equipment>
//     */
//    public function getEquipment(): Collection
//    {
//        return $this->equipment;
//    }
//
//    public function addEquipment(Equipment $equipment): self
//    {
//        if (!$this->equipment->contains($equipment)) {
//            $this->equipment->add($equipment);
//        }
//
//        return $this;
//    }
//
//    public function removeEquipment(Equipment $equipment): self
//    {
//        $this->equipment->removeElement($equipment);
//
//        return $this;
//    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Collection<int, OptionalSubject>
     */
    public function getOptionalSubjects(): Collection
    {
        return $this->optionalSubjects;
    }

    public function addOptionalSubject(OptionalSubject $optionalSubject): self
    {
        if (!$this->optionalSubjects->contains($optionalSubject)) {
            $this->optionalSubjects->add($optionalSubject);
            $optionalSubject->addRegistration($this);
        }

        return $this;
    }

    public function removeOptionalSubject(OptionalSubject $optionalSubject): self
    {
        if ($this->optionalSubjects->removeElement($optionalSubject)) {
            $optionalSubject->removeRegistration($this);
        }

        return $this;
    }
    #[Assert\Callback]
    public function verifyNbOptionalSubjects(ExecutionContextInterface $context, $payload) : void
    {
        if (count($this->optionalSubjects) > $this->getSchoolClass()->getSchoolLevel()->getNbDisciplineFacMax()) {
            $context->buildViolation("Vous ne pouvez choisir que {$this->getSchoolClass()->getSchoolLevel()->getNbDisciplineFacMax()} matière(s) facultative(s) maximum.")
                ->atPath('optionalSubjects')
                ->addViolation();
        }
    }
}
