<?php

namespace App\Entity\Users\Students;

use App\Entity\Users\User;
use App\Repository\UsersRepositories\StudentsRepositories\StudentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: StudentRepository::class)]
class Student extends User
{
    #[ORM\Column(length: 255)]
    private ?string $ine = null;

    #[ORM\OneToMany(mappedBy: 'student', targetEntity: Registration::class)]
    private Collection $registrations;

    #[ORM\OneToMany(mappedBy: 'student', targetEntity: LegalResponsibleStudent::class)]
    private Collection $legalResponsibleStudents;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Registration $currentRegistration = null;

    public function __construct()
    {
        parent::__construct();
        $this->registrations = new ArrayCollection();
        $this->setDisabled(false);
        $this->setRoles(['ROLE_STUDENT']);
        $this->legalResponsibleStudents = new ArrayCollection();
    }

    #[Assert\Callback]
    public function verifyNbOptionalSubjects(ExecutionContextInterface $context, $payload) : void
    {
        if (count($this->getLegalResponsibleStudents()->getValues()) >= 3) {
            $context->buildViolation("Un �tudiant ne peut pas avoir plus de 2 responsables l�gaux.")
                ->atPath('legalResponsibles')
                ->addViolation();
        }
    }

    public function getIne(): ?string
    {
        return $this->ine;
    }

    public function setIne(string $ine): self
    {
        $this->ine = $ine;

        return $this;
    }

    /**
     * @return Collection<int, Registration>
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations->add($registration);
            $this->setCurrentRegistration($registration);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getStudent() === $this) {
                $registration->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, LegalResponsibleStudent>
     */
    public function getLegalResponsibleStudents(): Collection
    {
        return $this->legalResponsibleStudents;
    }

    public function addLegalResponsibleStudent(LegalResponsibleStudent $legalResponsibleStudent): self
    {
        if (!$this->legalResponsibleStudents->contains($legalResponsibleStudent)) {
            $this->legalResponsibleStudents->add($legalResponsibleStudent);
            $legalResponsibleStudent->setStudent($this);
        }

        return $this;
    }

    public function removeLegalResponsibleStudent(LegalResponsibleStudent $legalResponsibleStudent): self
    {
        if ($this->legalResponsibleStudents->removeElement($legalResponsibleStudent)) {
            // set the owning side to null (unless already changed)
            if ($legalResponsibleStudent->getStudent() === $this) {
                $legalResponsibleStudent->setStudent(null);
            }
        }

        return $this;
    }

    public function getCurrentRegistration(): ?Registration
    {
        return $this->currentRegistration;
    }

    public function setCurrentRegistration(Registration $currentRegistration): static
    {
        $this->currentRegistration = $currentRegistration;

        return $this;
    }
}
