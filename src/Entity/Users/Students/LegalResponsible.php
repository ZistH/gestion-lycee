<?php

namespace App\Entity\Users\Students;

use App\Entity\Users\User;
use App\Repository\UsersRepositories\StudentsRepositories\LegalResponsibleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LegalResponsibleRepository::class)]
class LegalResponsible extends User
{
    #[ORM\OneToMany(mappedBy: 'legalresponsible', targetEntity: LegalResponsibleStudent::class)]
    private Collection $legalResponsibleStudents;

    public function __construct()
    {
        parent::__construct();
        $this->setDisabled(false);
        $this->setRoles(['ROLE_LEGAL_RESPONSIBLE']);
        $this->legalResponsibleStudents = new ArrayCollection();
    }

    /**
     * @return Collection<int, LegalResponsibleStudent>
     */
    public function getLegalResponsibleStudents(): Collection
    {
        return $this->legalResponsibleStudents;
    }

    public function addLegalResponsibleStudent(LegalResponsibleStudent $legalResponsibleStudent): self
    {
        if (!$this->legalResponsibleStudents->contains($legalResponsibleStudent)) {
            $this->legalResponsibleStudents->add($legalResponsibleStudent);
            $legalResponsibleStudent->setLegalresponsible($this);
        }

        return $this;
    }

    public function removeLegalResponsibleStudent(LegalResponsibleStudent $legalResponsibleStudent): self
    {
        if ($this->legalResponsibleStudents->removeElement($legalResponsibleStudent)) {
            // set the owning side to null (unless already changed)
            if ($legalResponsibleStudent->getLegalresponsible() === $this) {
                $legalResponsibleStudent->setLegalresponsible(null);
            }
        }

        return $this;
    }
}
