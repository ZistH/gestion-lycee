<?php

namespace App\Entity\Users;

use App\Repository\UsersRepositories\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    #[Assert\Regex(pattern: "#^[1-9]([0-9]+)?$#",
        message: "Le numéro de rue doit être composé de chiffres")]
    private ?string $streetNumber = null;

    #[ORM\Column(nullable: true)]
    #[Assert\Regex(pattern: "#^[0-9]([0-9]+)?$#",
        message: "Le numéro d'appartement doit être composé de chiffres")]
    private ?string $flatNumber = null;

    #[ORM\Column(length: 255)]
    private ?string $streetName = null;

    #[ORM\Column]
    #[Assert\Regex(pattern: "#^[0-9]{1}\d{4}$#",
        message: "Le code postal doit être composé de exactement 5 chiffres")]
    private ?string $postalcode = null;

    #[ORM\OneToMany(mappedBy: 'address', targetEntity: User::class)]
    private Collection $users;

    #[ORM\Column(length: 255)]
    #[Assert\Regex(pattern: "#^[a-zA-Z\-]+$#",
        message: "Un nom de ville ne doit pas contenir de chiffres.")]
    private ?string $town = null;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->streetName . ' ' . $this->postalcode;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFlatNumber(): ?string
    {
        return $this->flatNumber;
    }

    public function setFlatNumber(?string $flatNumber): self
    {
        $this->flatNumber = $flatNumber;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getPostalcode(): ?string
    {
        return $this->postalcode;
    }

    public function setPostalcode(string $postalcode): self
    {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setAddress($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getAddress() === $this) {
                $user->setAddress(null);
            }
        }

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }
}
