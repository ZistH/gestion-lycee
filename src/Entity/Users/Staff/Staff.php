<?php

namespace App\Entity\Users\Staff;

use App\Entity\Users\User;
use App\Repository\UsersRepositories\StaffRepositories\StaffRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StaffRepository::class)]
class Staff extends User
{
    public function __construct()
    {
        parent::__construct();
    }
}
