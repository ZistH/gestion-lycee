<?php

namespace App\Entity\Users\Staff;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Subjects\Subject;
use App\Repository\UsersRepositories\StaffRepositories\TeachingContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeachingContractRepository::class)]
class TeachingContract
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(cascade: ['persist',], inversedBy: 'teachingContract')]
    private ?Teacher $teacher = null;

    #[ORM\ManyToMany(targetEntity: SchoolClass::class, mappedBy: 'contract')]
    private Collection $schoolClasses;

    #[ORM\Column(name: "YEAR")]
    private ?int $date = null;

    #[ORM\ManyToOne(inversedBy: 'teachingContracts')]
    private ?Subject $subject = null;


    public function __construct()
    {

        $this->schoolClasses = new ArrayCollection();
        $this->date = date_format(new \DateTime(), "Y");
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeacher(): ?Teacher
    {
        return $this->teacher;
    }

    public function setTeacher(?Teacher $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return Collection<int, SchoolClass>
     */
    public function getSchoolClasses(): Collection
    {
        return $this->schoolClasses;
    }

    public function addSchoolClass(SchoolClass $schoolClass): self
    {
        if (!$this->schoolClasses->contains($schoolClass)) {
            $this->schoolClasses->add($schoolClass);
            $schoolClass->addContract($this);
        }

        return $this;
    }

    public function removeSchoolClass(SchoolClass $schoolClass): self
    {
        if ($this->schoolClasses->removeElement($schoolClass)) {
            $schoolClass->removeContract($this);
        }

        return $this;
    }

    public function getDate(): ?int
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = date_format($date, "Y");

        return $this;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

}
