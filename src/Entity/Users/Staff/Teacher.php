<?php

namespace App\Entity\Users\Staff;

use App\Entity\Users\User;
use App\Repository\UsersRepositories\StaffRepositories\TeacherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TeacherRepository::class)]
class Teacher extends User
{

    #[ORM\OneToMany(mappedBy: 'teacher', targetEntity: TeachingContract::class,)]
    private Collection $teachingContract;

    public function __construct()
    {
        parent::__construct();
        $this->teachingContract = new ArrayCollection();

    }
    /**
     * @return Collection<int, TeachingContract>
     */
    public function getTeachingContract(): Collection
    {
        return $this->teachingContract;
    }

    public function addTeachingContract(TeachingContract $teachingContract): self
    {
        if (!$this->teachingContract->contains($teachingContract)) {
            $this->teachingContract->add($teachingContract);
            $teachingContract->setTeacher($this);
        }

        return $this;
    }

    public function removeTeachingContract(TeachingContract $teachingContract): self
    {
        if ($this->teachingContract->removeElement($teachingContract)) {
            // set the owning side to null (unless already changed)
            if ($teachingContract->getTeacher() === $this) {
                $teachingContract->setTeacher(null);
            }
        }

        return $this;
    }
}
