<?php

namespace App\Entity\Subjects;

use App\Entity\Users\Staff\TeachingContract;
use App\Repository\SubjectsRepositories\SubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class représentant une matière
 */

#[ORM\Entity(repositoryClass: SubjectRepository::class)]
#[ORM\InheritanceType("JOINED")]
#[ORM\DiscriminatorColumn(name: "type", type: "string")]
#[ORM\DiscriminatorMap([
    'commonSubject' => CommonSubject::class,
    'optional' => OptionalSubject::class,
    'language' => Language::class
])]
abstract class Subject
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?bool $disabled = null;

    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: TeachingContract::class)]
    private Collection $teachingContracts;

    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: IsTaught::class)]
    private Collection $areTaught;


    public function __construct()
    {
        $this->teachingContracts = new ArrayCollection();
        $this->areTaught = new ArrayCollection();
        $this->setDisabled(false);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * @return Collection<int, TeachingContract>
     */
    public function getTeachingContracts(): Collection
    {
        return $this->teachingContracts;
    }

    public function addTeachingContract(TeachingContract $teachingContract): self
    {
        if (!$this->teachingContracts->contains($teachingContract)) {
            $this->teachingContracts->add($teachingContract);
            $teachingContract->setSubject($this);
        }

        return $this;
    }

    public function removeTeachingContract(TeachingContract $teachingContract): self
    {
        if ($this->teachingContracts->removeElement($teachingContract)) {
            // set the owning side to null (unless already changed)
            if ($teachingContract->getSubject() === $this) {
                $teachingContract->setSubject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IsTaught>
     */
    public function getAreTaught(): Collection
    {
        return $this->areTaught;
    }

    public function addIsTaught(IsTaught $isTaught): self
    {
        if (!$this->areTaught->contains($isTaught)) {
            $this->areTaught->add($isTaught);
            $isTaught->setSubject($this);
        }

        return $this;
    }

    public function removeIsTaught(IsTaught $isTaught): self
    {
        if ($this->areTaught->removeElement($isTaught)) {
            // set the owning side to null (unless already changed)
            if ($isTaught->getSubject() === $this) {
                $isTaught->setSubject(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}
