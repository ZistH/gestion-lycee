<?php

namespace App\Entity\Subjects;

use App\Entity\Users\Students\Registration;
use App\Repository\SubjectsRepositories\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LanguageRepository::class)]
class Language extends Subject
{

    #[ORM\OneToMany(mappedBy: 'language', targetEntity: Registration::class, orphanRemoval: true)]
    private Collection $registration;


    public function __construct()
    {
        parent::__construct();
        $this->registration = new ArrayCollection();
        $this->setDisabled(false);
    }

    /**
     * @return Collection<int, registration>
     */
    public function getRegistration(): Collection
    {
        return $this->registration;
    }

    public function addRegistration(registration $registration): self
    {
        if (!$this->registration->contains($registration)) {
            $this->registration->add($registration);
            $registration->setLanguage($this);
        }

        return $this;
    }

    public function removeRegistration(registration $registration): self
    {
        if ($this->registration->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getLanguage() === $this) {
                $registration->setLanguage(null);
            }
        }

        return $this;
    }

}
