<?php

namespace App\Entity\Subjects;

use App\Entity\Users\Students\Registration;
use App\Repository\SubjectsRepositories\OptionalSubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OptionalSubjectRepository::class)]
class OptionalSubject extends Subject
{
    #[ORM\ManyToMany(targetEntity: Registration::class, inversedBy: 'optionalSubjects')]
    private Collection $registrations;

    public function __construct()
    {
        parent::__construct();
        $this->registrations = new ArrayCollection();
    }

    /**
     * @return Collection<int, Registration>
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations->add($registration);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        $this->registrations->removeElement($registration);

        return $this;
    }
}
