<?php

namespace App\Entity\Subjects;

use App\Repository\SubjectsRepositories\CommonSubjectRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommonSubjectRepository::class)]
class CommonSubject extends Subject
{

}