<?php

namespace App\Entity\Subjects;

use App\Entity\Equipments\Equipment;
use App\Entity\SchoolClasses\SchoolLevel;
use App\Repository\SubjectsRepositories\IsTaughtRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class qui permet de représenter la relation entre
 * les équipements, les matières et les niveaux d'enseignement.
 */
#[ORM\Entity(repositoryClass: IsTaughtRepository::class)]
class IsTaught
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'areTaught')]
    private ?SchoolLevel $schoolLevel = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'areTaught')]
    #[Assert\Valid]
    private ?Subject $subject = null;

//    #[ORM\ManyToMany(targetEntity: Equipment::class, inversedBy: 'areTaught')]
//    private Collection $equipments;

    public function __construct()
    {
//        $this->equipments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchoolLevel(): ?SchoolLevel
    {
        return $this->schoolLevel;
    }

    public function setSchoolLevel(?SchoolLevel $schoolLevel): self
    {
        $this->schoolLevel = $schoolLevel;

        return $this;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return Collection<int, Equipment>
     */
//    public function getEquipments(): Collection
//    {
//        return $this->equipments;
//    }
//
//    public function addEquipment(Equipment $equipment): self
//    {
//        if (!$this->equipments->contains($equipment)) {
//            $this->equipments->add($equipment);
//        }
//
//        return $this;
//    }
//
//    public function removeEquipment(Equipment $equipment): self
//    {
//        $this->equipments->removeElement($equipment);
//
//        return $this;
//    }
}
