<?php

namespace App\Entity\SchoolClasses;

use App\Entity\Users\Students\Registration;
use App\Entity\Users\Staff\TeachingContract;
use App\Repository\SchoolClassesRepositories\SchoolClassRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: SchoolClassRepository::class)]
class SchoolClass
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\OneToMany(mappedBy: 'schoolClass', targetEntity: Registration::class)]
    private Collection $registrations;
    #[ORM\Column]
    #[Assert\LessThanOrEqual(value: 40)]
    #[Assert\GreaterThan(value: 0)]
    private ?int $maxRegistrations;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: TeachingContract::class, inversedBy: 'schoolClasses')]
    private Collection $contract;

    #[ORM\ManyToOne(inversedBy: 'schoolClasses')]
    private ?SchoolLevel $schoolLevel = null;


    public function __construct()
    {
        $this->registrations = new ArrayCollection();
        $this->contract = new ArrayCollection();
        $this->maxRegistrations = 40;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Registration>
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations->add($registration);
            $registration->setSchoolClass($this);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getSchoolClass() === $this) {
                $registration->setSchoolClass(null);
            }
        }

        return $this;
    }

    public function getMaxRegistrations(): ?int
    {
        return $this->maxRegistrations;
    }

    public function setMaxRegistrations(int $maxRegistrations): self
    {
        $this->maxRegistrations = $maxRegistrations;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, TeachingContract>
     */
    public function getContract(): Collection
    {
        return $this->contract;
    }

    public function addContract(TeachingContract $contract): self

    {
        if (!$this->contract->contains($contract)) {
            $this->contract->add($contract);
        }

        return $this;
    }

    public function removeContract(TeachingContract $contract): self
    {
        $this->contract->removeElement($contract);

        return $this;
    }

    public function getSchoolLevel(): ?SchoolLevel
    {
        return $this->schoolLevel;
    }

    public function setSchoolLevel(?SchoolLevel $schoolLevel): self
    {
        $this->schoolLevel = $schoolLevel;

        return $this;
    }


    public function __toString(): string
    {
        return $this->getName();
    }
}
