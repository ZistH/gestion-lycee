<?php

namespace App\Entity\SchoolClasses;

use App\Entity\Subjects\IsTaught;
use App\Repository\SchoolClassesRepositories\SchoolLevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SchoolLevelRepository::class)]
class SchoolLevel
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $name = null;

    #[ORM\Column]
    #[Assert\LessThanOrEqual(value: 3)]
    #[Assert\GreaterThan(value: -1)]
    private ?int $NbDisciplineFacMax = null;

    #[ORM\OneToMany(mappedBy: 'schoolLevel', targetEntity: SchoolClass::class)]
    private Collection $schoolClasses;

    #[ORM\OneToMany(mappedBy: 'schoolLevel', targetEntity: IsTaught::class)]
    private Collection $areTaught;

    public function __construct()
    {
        $this->schoolClasses = new ArrayCollection();
        $this->areTaught = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNbDisciplineFacMax(): ?int
    {
        return $this->NbDisciplineFacMax;
    }

    /**
     * @param int|null $NbDisciplineFacMax
     * @return SchoolLevel
     */
    public function setNbDisciplineFacMax(?int $NbDisciplineFacMax): self
    {
        $this->NbDisciplineFacMax = $NbDisciplineFacMax;
        return $this;
    }

    /**
     * @return Collection<int, SchoolClass>
     */
    public function getSchoolClasses(): Collection
    {
        return $this->schoolClasses;
    }

    public function addSchoolClass(SchoolClass $schoolClass): self
    {
        if (!$this->schoolClasses->contains($schoolClass)) {
            $this->schoolClasses->add($schoolClass);
            $schoolClass->setSchoolLevel($this);
        }

        return $this;
    }

    public function removeSchoolClass(SchoolClass $schoolClass): self
    {
        if ($this->schoolClasses->removeElement($schoolClass)) {
            // set the owning side to null (unless already changed)
            if ($schoolClass->getSchoolLevel() === $this) {
                $schoolClass->setSchoolLevel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, IsTaught>
     */
    public function getAreTaught(): Collection
    {
        return $this->areTaught;
    }

    public function addIsTaught(IsTaught $isTaught): self
    {
        if (!$this->areTaught->contains($isTaught)) {
            $this->areTaught->add($isTaught);
            $isTaught->setSchoolLevel($this);
        }

        return $this;
    }

    public function removeIsTaught(IsTaught $isTaught): self
    {
        if ($this->areTaught->removeElement($isTaught)) {
            // set the owning side to null (unless already changed)
            if ($isTaught->getSchoolLevel() === $this) {
                $isTaught->setSchoolLevel(null);
            }
        }

        return $this;
    }


    public function __toString() : string
    {
        return $this->name;
    }

}
