<?php
//
//namespace App\Entity\Equipments;
//
//use App\Repository\EquipmentsRepositories\EquipmentTypeRepository;
//use Doctrine\ORM\Mapping as ORM;
//
//#[ORM\Entity(repositoryClass: EquipmentTypeRepository::class)]
//#[ORM\Table(name: "equipment_type")]
//class TypeEquipment
//{
//    #[ORM\Id]
//    #[ORM\GeneratedValue]
//    #[ORM\Column]
//    private ?int $id = null;
//
//    #[ORM\Column(length: 255, unique: true)]
//    private ?string $type = null;
//
//    public function getId(): ?int
//    {
//        return $this->id;
//    }
//
//    public function getType(): ?string
//    {
//        return $this->type;
//    }
//
//    public function setType(string $type): self
//    {
//        $this->type = $type;
//
//        return $this;
//    }
//}
