<?php
//
//namespace App\Entity\Equipments;
//
//use App\Entity\Subjects\IsTaught;
//use App\Entity\Users\Students\Registration;
//use App\Repository\EquipmentsRepositories\EquipmentRepository;
//use Doctrine\Common\Collections\ArrayCollection;
//use Doctrine\Common\Collections\Collection;
//use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Validator\Constraints as Assert;
//
//#[ORM\Entity(repositoryClass: EquipmentRepository::class)]
//class Equipment
//{
//    #[ORM\Id]
//    #[ORM\GeneratedValue]
//    #[ORM\Column]
//    private ?int $id = null;
//
//    #[ORM\Column]
//    private ?bool $reusable = null;
//
//    #[ORM\Column]
//    private ?float $cost = null;
//
//    #[ORM\Column(length: 255, nullable: true)]
//    private ?string $title = null;
//
//    #[ORM\Column(length: 255, nullable: true)]
//    private ?string $author = null;
//
//    #[ORM\Column(nullable: true)]
//    private ?int $yearOfPublication = null;
//
//    #[ORM\ManyToOne]
//    private ?TypeEquipment $type = null;
//
//    #[ORM\ManyToMany(targetEntity: IsTaught::class, mappedBy: 'equipments')]
//    private Collection $areTaught;
//
//    #[ORM\ManyToMany(targetEntity: Registration::class, mappedBy: 'equipment')]
//    private Collection $registrations;
//
//    #[ORM\Column(length: 15, nullable: true)]
//    #[Assert\Regex(pattern: "#^\d{6}$#",
//        message: "L'ISBN doit être composé de exactement 13 chiffres")]
//    private ?string $isbn = null;
//
//
//    public function __construct()
//    {
//        $this->areTaught = new ArrayCollection();
//        $this->registrations = new ArrayCollection();
//    }
//
//
//    public function getId(): ?int
//    {
//        return $this->id;
//    }
//
//
//    public function isReusable(): ?bool
//    {
//        return $this->reusable;
//    }
//
//    public function setReusable(bool $reusable): self
//    {
//        $this->reusable = $reusable;
//
//        return $this;
//    }
//
//    public function getCost(): ?float
//    {
//        return $this->cost;
//    }
//
//    public function setCost(float $cost): self
//    {
//        $this->cost = $cost;
//
//        return $this;
//    }
//
//    public function getTitle(): ?string
//    {
//        return $this->title;
//    }
//
//    public function setTitle(?string $title): self
//    {
//        $this->title = $title;
//
//        return $this;
//    }
//
//    public function getAuthor(): ?string
//    {
//        return $this->author;
//    }
//
//    public function setAuthor(?string $author): self
//    {
//        $this->author = $author;
//
//        return $this;
//    }
//
//    public function getYearOfPublication(): ?int
//    {
//        return $this->yearOfPublication;
//    }
//
//    public function setYearOfPublication(?int $yearOfPublication): self
//    {
//        $this->yearOfPublication = intval($yearOfPublication);
//
//        return $this;
//    }
//
//    public function getType(): ?TypeEquipment
//    {
//        return $this->type;
//    }
//
//    public function setType(?TypeEquipment $type): self
//    {
//        $this->type = $type;
//
//        return $this;
//    }
//
//    public function __toString(): string
//    {
//        return $this->type->getType() . ': ' . $this->title;
//    }
//
//    /**
//     * @return Collection<int, IsTaught>
//     */
//    public function getAreTaught(): Collection
//    {
//        return $this->areTaught;
//    }
//
//    public function addIsTaught(IsTaught $isTaught): self
//    {
//        if (!$this->areTaught->contains($isTaught)) {
//            $this->areTaught->add($isTaught);
//            $isTaught->addEquipment($this);
//        }
//
//        return $this;
//    }
//
//    public function removeIsTaught(IsTaught $isTaught): self
//    {
//        if ($this->areTaught->removeElement($isTaught)) {
//            $isTaught->removeEquipment($this);
//        }
//
//        return $this;
//    }
//
//
//    /**
//     * @return Collection<int, Registration>
//     */
//    public function getRegistrations(): Collection
//    {
//        return $this->registrations;
//    }
//
//    public function addRegistration(Registration $registration): self
//    {
//        if (!$this->registrations->contains($registration)) {
//            $this->registrations->add($registration);
//            $registration->addEquipment($this);
//        }
//
//        return $this;
//    }
//
//    public function removeRegistration(Registration $registration): self
//    {
//        if ($this->registrations->removeElement($registration)) {
//            $registration->removeEquipment($this);
//        }
//
//        return $this;
//    }
//
//    public function getIsbn(): ?string
//    {
//        return $this->isbn;
//    }
//
//    public function setIsbn(?string $isbn): self
//    {
//        $this->isbn = $isbn;
//
//        return $this;
//    }
//}
