<?php

namespace App\Twig\Extension;

use App\Entity\Subjects\CommonSubject;
use App\Entity\Subjects\Language;
use App\Entity\Subjects\OptionalSubject;
use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class TestInstanceOfSubject extends AbstractExtension
{
    public function getTests() : array
    {
        return [
            new TwigTest('instanceOfOptionalSubject', [$this, 'isInstanceOfOptionalSubject']),
            new TwigTest('isInstanceOfCommonSubject', [$this, 'isInstanceOfCommonSubject']),
            new TwigTest('isInstanceOfLanguage', [$this, 'isInstanceOfLanguage']),
        ];
    }

    public function isInstanceOfOptionalSubject($var) :bool
    {
        return $var instanceof OptionalSubject;
    }
    public function isInstanceOfCommonSubject($var) :bool
    {
        return $var instanceof CommonSubject;
    }
    public function isInstanceOfLanguage($var) :bool
    {
        return $var instanceof Language;
    }
}