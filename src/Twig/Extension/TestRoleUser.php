<?php

namespace App\Twig\Extension;

use App\Entity\Users\User;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TestRoleUser extends AbstractExtension
{
    public function getFunctions() : array
    {
        return [
            new TwigFunction('isStudent', [$this, 'isStudent']),
            new TwigFunction('isTeacher', [$this, 'isTeacher']),
            new TwigFunction('isRh', [$this, 'isRh']),
            new TwigFunction('isDocumentation', [$this, 'isDocumentation']),
            new TwigFunction('isStaff', [$this, 'isStaff']),
            new TwigFunction('isScolarite', [$this, 'isScolarite']),
            new TwigFunction('isLegalRespoonsible', [$this, 'isLegalRespoonsible']),
        ];
    }

    public function isStudent(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_STUDENT", $roles)) {
            return true;
        }

        return false;
    }

    public function isTeacher(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_TEACHER", $roles)) {
            return true;
        }

        return false;
    }

    public function isRh(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_RH", $roles)) {
            return true;
        }

        return false;
    }

    public function isScolarite(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_SCOLARITE", $roles)) {
            return true;
        }

        return false;
    }

    public function isDocumentation(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_DOCUMENTATION", $roles)) {
            return true;
        }

        return false;
    }

    public function isStaff(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_STAFF", $roles)) {
            return true;
        }

        return false;
    }

    public function isLegalRespoonsible(User $user) : bool
    {
        $roles = $user->getRoles();

        if (in_array("ROLE_LEGAL_RESPONSIBLE", $roles)) {
            return true;
        }

        return false;
    }
}