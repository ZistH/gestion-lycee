<?php

namespace App\Form;

use App\Entity\SchoolClasses\SchoolLevel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationSchoolLevelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('schoolLevel', EntityType::class, [
                'class' => SchoolLevel::class,
                'label' => 'Niveau scolaire',
                 'choice_label' => 'name'
            ])
            ->add('send', SubmitType::class, ['label' => 'Valider le niveau de l\'inscription.']);

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([

        ]);
    }
}
