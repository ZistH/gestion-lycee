<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password',TextType::class, ['label'=>'Ancien mot de passe','mapped'=> false])
            ->add('newPassword',TextType::class, ['label'=>'Nouveau mot de passe','mapped'=> false])
            ->add('newPassword2',TextType::class, ['label'=>'Confirmer nouveau mot de passe','mapped'=> false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}
