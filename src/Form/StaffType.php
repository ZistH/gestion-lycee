<?php

namespace App\Form;

use App\Entity\Users\Staff\Staff;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StaffType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label'=>"Nom"])
            ->add('firstname', TextType::class, ['label'=>"Prénom"])
            ->add('email', EmailType::class, ['label'=>"Adresse mail"])
            ->add('telephone', TextType::class, ['label'=>"Téléphone", 'required'=>false])
            ->add('address', AdressType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Staff::class,
        ]);
    }
}
