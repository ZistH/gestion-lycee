<?php

namespace App\Form;

use App\Entity\Equipments\Equipment;
use App\Entity\SchoolClasses\SchoolLevel;
use App\Entity\Subjects\IsTaught;
use App\Entity\Subjects\Subject;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IsTaughtType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('schoolLevel', EntityType::class, [
                'class' => SchoolLevel::class,
                'choice_label' => 'name',
                'label' => 'Niveau Scolaire',
            ])
            ->add('subject', SubjectType::class, ['data_class'=> Subject::class])
//            ->add('equipments', EntityType::class, [
//                'class' => Equipment::class,
//                'mapped' => true,
//                'by_reference' => false,
//                'multiple' => true,
//                'expanded' => true
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => IsTaught::class,
        ]);
    }
}
