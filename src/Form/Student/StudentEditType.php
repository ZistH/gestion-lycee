<?php

namespace App\Form\Student;

use App\Entity\Users\Students\Student;
use App\Form\AdressType;
use App\Form\LegalResponsible\LegalResponsibleEditType;
use App\Form\LegalResponsible\LegalResponsibleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ine', TextType::class, ['label'=>"INE", 'required' => false, 'disabled'=>true])
            ->add('name', TextType::class, ['label'=>"Nom"])
            ->add('firstname', TextType::class, ['label'=>"Prénom"])
            ->add('email', EmailType::class, ['label'=>"Email"])
            ->add('telephone', TextType::class, [
                'label'=>"Téléphone",
                'required'=>false,
                'attr' => ['placeholder'=>'format : "01 01 01 01 01" ou "0101010101"']
            ])
            ->add('address', AdressType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
