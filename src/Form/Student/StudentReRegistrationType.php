<?php

namespace App\Form\Student;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\Students\Student;
use App\Form\AdressType;
use App\Form\LegalResponsible\LegalResponsibleType;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentReRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label'=>"Nom"])
            ->add('firstname', TextType::class, ['label'=>"Prénom"])
            ->add('ine', TextType::class, ['label'=>"INE"])
            ->add('email', EmailType::class, ['label'=>"Email"])
            ->add('telephone', TextType::class, ['label'=>"Téléphone",
                'required'=>false
            ])
            ->add('legalResponsible1', LegalResponsibleType::class, ['label'=>"Responsable légal 1",
                'by_reference' => false,
                'required' => true,
                'mapped'=> false,
                'disabled'=>true
            ])
            ->add('legalResponsible2', LegalResponsibleType::class, ['label'=>"Responsable légal 2",
                'required'=>false,
                'by_reference' => false,
                'mapped'=> false,
                'disabled'=>true
            ])
            ->add('address', AdressType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
