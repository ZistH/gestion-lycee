<?php

namespace App\Form\Student;

use App\Entity\Users\Students\Student;
use App\Form\AdressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentFromLegalResponsibleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label'=>"Nom"])
            ->add('firstname', TextType::class, ['label'=>"Prénom"])
            ->add('ine', TextType::class, ['label'=>"INE"])
            ->add('email', EmailType::class, ['label'=>"Email"])
            ->add('telephone', TextType::class, ['label'=>"Téléphone", 'required'=>false])
            ->add('address', AdressType::class)
//            ->add('class', EntityType::class, [
//                'class' => SchoolClass::class,
////                'choice_label' => 'id'
//                'mapped' => false
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
