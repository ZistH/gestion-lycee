<?php

namespace App\Form\Student;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\Students\LegalResponsibleStudent;
use App\Entity\Users\Students\Student;
use App\Form\AdressType;
use App\Form\LegalResponsible\LegalResponsibleType;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label'=>"Nom"])
            ->add('firstname', TextType::class, ['label'=>"Prénom"])
            ->add('ine', TextType::class, ['label'=>"INE"])
            ->add('email', EmailType::class, ['label'=>"Email"])
            ->add('telephone', TextType::class, ['label'=>"Téléphone",
                'required'=>false
            ])
//            ->add('', LegalResponsibleStudentType::class, ['label'=>"Responsable légal 1",
//                'required' => true,
//                'by_reference' => false,
//                //'mapped'=> false
//            ])
//            ->add('legalResponsible2', LegalResponsibleStudentType::class, ['label'=>"Responsable légal 2",
//                'required'=>false,
//                'by_reference' => false,
//                //'mapped'=> false
//            ])
            ->add('address', AdressType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
