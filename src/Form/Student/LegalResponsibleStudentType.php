<?php

namespace App\Form\Student;

use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\Students\LegalResponsibleStudent;
use App\Form\LegalResponsible\LegalResponsibleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LegalResponsibleStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('legalresponsible', LegalResponsibleType::class,[ 'label' => 'Responsable legal'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => LegalResponsibleStudent::class,
        ]);
    }
}
