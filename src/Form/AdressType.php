<?php

namespace App\Form;

use App\Entity\Users\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('streetNumber', TextType::class, ['label' => 'Numéro de rue', 'required' => true])
            ->add('flatNumber', TextType::class, ['label' => 'Numéro d\'appartement', 'required' => false])
            ->add('streetName', TextType::class, ['label' => 'Rue'])
            ->add('town', TextType::class, ['label' => 'Ville'])
            ->add('postalcode', TextType::class, ['label' => 'Code postal']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
