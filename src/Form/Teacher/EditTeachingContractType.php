<?php

namespace App\Form\Teacher;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Subjects\Subject;
use App\Entity\Users\Staff\TeachingContract;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditTeachingContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('teacher', TeacherType::class, [
                'mapped'=>true
            ])
            ->add('schoolclasses', EntityType::class, [
                'class' => SchoolClass::class,
                'label' => 'Classes',
//                'choice_label' => 'name',
                'by_reference' => false,
                'multiple' => true,
                'expanded' => true,
                'required' => true
            ])
            ->add('subject', EntityType::class, [
                'class' => Subject::class,
                'choice_label' => 'name',
                'label'=> 'Matière',
            ])
//            ->add('user', EntityType::class, [
//                'class' => User::class,
//                'choice_label' => 'name',
//                'label'=> 'Matière',
//                'mapped'=>false
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TeachingContract::class,
        ]);
    }
}
