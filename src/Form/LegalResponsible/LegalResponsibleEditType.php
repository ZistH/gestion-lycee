<?php

namespace App\Form\LegalResponsible;

use App\Entity\Users\Students\LegalResponsible;
use App\Form\AdressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LegalResponsibleEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label'=>"Nom"])
            ->add('firstname', TextType::class, ['label'=>"Prénom"])
            ->add('email', EmailType::class, ['label'=>"Email"])
            ->add('telephone', TextType::class, ['label'=>"Téléphone", 'required'=>false])
            ->add('address', AdressType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => LegalResponsible::class,
        ]);
    }
}
