<?php

namespace App\Form;

use App\Entity\Equipments\Equipment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EquipmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reusable', CheckboxType::class,['label' => 'Réutilisable', 'required' => false])
            ->add('cost', NumberType::class, ['label' => 'Coût'])
            ->add('title', TextType::class, ['label'=>'Titre / Nom'])
            ->add('author', TextType::class, ['label'=>'Auteur / Marque'])
            ->add('isbn',TextType::class, ['label'=>'ISBN', 'required' => false])
            ->add('yearOfPublication', IntegerType::class, [
                'label'=>'Année de publication',
                'attr' => [
                    'min' => 2000,
                    'max' => date_format(new \DateTime, "Y"),
                    'value' => null

                ],
                'required' => false
            ])
//            ->add('statement', StatementType::class, ['label' => 'Etat'])

//            ->add('type', TypeEquipmentType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Equipment::class,
        ]);
    }
}
