<?php

namespace App\Form\Registration;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Subjects\Language;
use App\Entity\Users\Students\Registration;
use App\Form\Student\StudentEditType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFromLegalResponsibleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('student', StudentEditType::class)
            ->add('schoolClass', EntityType::class, [
                'class' => SchoolClass::class,
                'choice_label' => 'name'
            ])
            ->add('language',EntityType::class,[
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => 'Langue'

            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Registration::class
        ]);
    }
}
