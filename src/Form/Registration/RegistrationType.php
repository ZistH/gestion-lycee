<?php

namespace App\Form\Registration;

use App\Entity\Subjects\Language;
use App\Entity\Users\Students\Registration;
use App\Form\Student\LegalResponsibleStudentType;
use App\Form\Student\StudentType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('student', StudentType::class)

            ->add('language',EntityType::class,[
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => 'Langue'
            ])

            ->add('send', SubmitType::class, ['label' => 'Valider l\'inscription.']);

            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Registration::class
        ]);
    }
}
