<?php

namespace App\Form;

use App\Entity\SchoolClasses\SchoolLevel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SchoolLevelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du niveau'
            ])
            ->add('NbDisciplineFacMax', IntegerType::class, [
                'label' => 'Nombre maximum de discipline facultative possible.',
                'attr' => [
                    'min' => 0,
                    'max' => 3,
                ]
            ])
            ->add('send', SubmitType::class, [
                'label' => 'Valider'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SchoolLevel::class,
        ]);
    }
}
