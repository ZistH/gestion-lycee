<?php

namespace App\DataFixtures;


use App\Entity\Equipments\Equipment;
use App\Entity\Equipments\TypeEquipment;
use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\SchoolClasses\SchoolLevel;
use App\Entity\Subjects\CommonSubject;
use App\Entity\Subjects\IsTaught;
use App\Entity\Subjects\Language;
use App\Entity\Subjects\OptionalSubject;
use App\Entity\Subjects\Subject;
use App\Entity\Users\Address;
use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\Students\LegalResponsibleStudent;
use App\Entity\Users\Students\Registration;
use App\Entity\Users\Students\Student;
use App\Entity\Users\Staff\Staff;
use App\Entity\Users\Staff\Teacher;
use App\Entity\Users\Staff\TeachingContract;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 *  Class permettant de charger des données fictives en base de données pour les tests de développement
 */
class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $this->createSchoolClassAndLevel($manager);
//
        $this->createStaff($manager);
//
        $this->createSubjects($manager);
//
        $this->createTeacherAndTeachingContract($manager);
//
        $this->createStudentAndLegalResponsible($manager, 20);
//
//        //$this->createEquipmentsTypeAndEquipments($manager);
//
        $this->createIsTaught($manager);

        $manager->flush();
    }

    public function createSchoolClassAndLevel(ObjectManager $manager) : void
    {
        $schoolLevel1 = (new SchoolLevel())
            ->setName("Second")
            ->setNbDisciplineFacMax(3);
        $schoolLevel2 = (new SchoolLevel())
            ->setName("Première")
            ->setNbDisciplineFacMax(2);
        $schoolLevel3 = (new SchoolLevel())
            ->setName("Terminale")
            ->setNbDisciplineFacMax(1);
        $manager->persist($schoolLevel1);
        $manager->persist($schoolLevel2);
        $manager->persist($schoolLevel3);

        $class1 = (new SchoolClass())
            ->setSchoolLevel($schoolLevel1)
            ->setName($schoolLevel1->getName() . ' 1');
        $manager->persist($class1);

        $class2 = (new SchoolClass())
            ->setSchoolLevel($schoolLevel2)
            ->setName($schoolLevel2->getName() . ' 1');
        $manager->persist($class2);

        $class3 = (new SchoolClass())
            ->setSchoolLevel($schoolLevel3)
            ->setName($schoolLevel3->getName() . ' 1');
        $manager->persist($class3);

        $manager->flush();
    }

    public function createStaff(ObjectManager $manager) : void
    {
        $address = (new Address)
            ->setPostalcode(24523)
            ->setTown("Eysier")
            ->setStreetName("rue du Staff")
            ->setStreetNumber(1);
        $rh = (new Staff())
            ->setAddress($address)
            ->setDisabled(false)
            ->setEmail("roger.haris@mail.com")
            ->setFirstname("Roger")
            ->setName("Haris")
            ->setTelephone("07 98 76 " . rand(10, 99) . " " . rand(10, 99))
            ->setRoles(["ROLE_RH", "ROLE_STAFF"]);
        $manager->persist($rh);

        $address = (new Address)
            ->setPostalcode(24523)
            ->setTown("Eysier")
            ->setStreetName("rue du Staff")
            ->setStreetNumber(2);
        $scolarite = (new Staff())
            ->setAddress($address)
            ->setDisabled(false)
            ->setEmail("sandrine.colere@mail.com")
            ->setFirstname("Sandrine")
            ->setName("Colère")
            ->setTelephone("07 98 76 " . rand(10, 99) . " " . rand(10, 99))
            ->setRoles(["ROLE_SCOLARITE", "ROLE_STAFF"]);
        $manager->persist($scolarite);

        $address = (new Address)
            ->setPostalcode(24523)
            ->setTown("Eysier")
            ->setStreetName("rue du Staff")
            ->setStreetNumber(3);
        $documentation = (new Staff())
            ->setAddress($address)
            ->setDisabled(false)
            ->setEmail("dorian.occultant@mail.com")
            ->setFirstname("Dorian")
            ->setName("Occultant")
            ->setTelephone("07 98 76 " . rand(10, 99) . " " . rand(10, 99))
            ->setRoles(["ROLE_DOCUMENTATION", "ROLE_STAFF"]);
        $manager->persist($documentation);

        $manager->flush();
    }

    public function createSubjects(ObjectManager $manager) : void
    {
        $subject1 = (new CommonSubject)->setName("Français");
        $subject2 = (new CommonSubject)->setName('Mathématiques');
        $subject3 = (new CommonSubject)->setName('Histoire-Géographie');

        $manager->persist($subject1);
        $manager->persist($subject2);
        $manager->persist($subject3);

        $optionalSubject1 = (new OptionalSubject)->setName('Theatres');
        $optionalSubject2 = (new OptionalSubject)->setName('Musique');

        $manager->persist($optionalSubject1);
        $manager->persist($optionalSubject2);

        $language1 = (new Language)->setName('Anglais');
        $language2 = (new Language)->setName('Allemand');
        $language3 = (new Language)->setName('Espagnol');

        $manager->persist($language1);
        $manager->persist($language2);
        $manager->persist($language3);

        $manager->flush();
    }

    public function createEquipmentsTypeAndEquipments(ObjectManager $manager) : void
    {
        $equipmentType1 = (new TypeEquipment)->setType("Manuel");
        $equipmentType2 = (new TypeEquipment)->setType("Cahier de TD");
        $equipmentType3 = (new TypeEquipment)->setType("Instrument");

        $equipment1 = (new Equipment)
            ->setType($equipmentType1)
            ->setAuthor("Hachette")
            ->setCost(0)
            ->setIsbn("" . rand(1000, 9999))
            ->setTitle('Français')
            ->setReusable(true)
            ->setYearOfPublication(2020);

        $equipment2 = (new Equipment)
            ->setType($equipmentType1)
            ->setAuthor("Hachette")
            ->setCost(0)
            ->setIsbn("" . rand(1000, 9999))
            ->setTitle('Mathématiques')
            ->setReusable(true)
            ->setYearOfPublication(2020);

        $equipment3 = (new Equipment)
            ->setType($equipmentType2)
            ->setAuthor("Hachette")
            ->setCost(0)
            ->setIsbn("" . rand(1000, 9999))
            ->setTitle('Exercises de Mathématiques')
            ->setReusable(false)
            ->setYearOfPublication(2020);


        $equipment4 = (new Equipment)
            ->setType($equipmentType2)
            ->setCost(0)
            ->setTitle('Flûte')
            ->setReusable(false)
            ->setYearOfPublication(2020);

        $manager->persist($equipmentType1);
        $manager->persist($equipmentType2);
        $manager->persist($equipmentType3);

        $manager->persist($equipment1);
        $manager->persist($equipment2);
        $manager->persist($equipment3);
        $manager->persist($equipment4);

        $manager->flush();
    }

    public function createIsTaught(ObjectManager $manager) : void
    {
        $subjects = $manager->getRepository(Subject::class)->findAll();
        $schoolLevels = $manager->getRepository(SchoolLevel::class)->findAll();
        /*
            $equipmentRepository = $manager->getRepository(Equipment::class);
            $flute = $equipmentRepository->findOneBy(['title' => "Flûte"]);
            $exMath = $equipmentRepository->findOneBy(['title' => "Exercises de Mathématiques"]);
            $math = $equipmentRepository->findOneBy(['title' => "Mathématiques"]);
            $fr = $equipmentRepository->findOneBy(['title' => "Français"]);
        */
        foreach ($subjects as $subject){
            foreach ($schoolLevels as $schoolLevel){
                $isTaught = (new IsTaught)
                    ->setSchoolLevel($schoolLevel)
                    ->setSubject($subject)
                    ;
                /*
                if ($subject->getName() == 'Français'){
                    $isTaught->addEquipment($fr);
                }
                if ($subject->getName() == 'Mathématiques'){
                    $isTaught->addEquipment($math);
                    $isTaught->addEquipment($exMath);
                }
                if ($subject->getName() == 'Musique'){
                    $isTaught->addEquipment($flute);
                }
                */
                $manager->persist($isTaught);
                $manager->flush();
            }
        }
    }

    public function createTeacherAndTeachingContract(ObjectManager $manager) : void
    {
        $subjects = $manager->getRepository(Subject::class)->findAll();
        $schoolClasses = $manager->getRepository(SchoolClass::class)->findAll();

        foreach ($subjects as $subject) {
            $address = (new Address)
                ->setPostalcode(rand(10000,99999))
                ->setTown("Eysier")
                ->setStreetName("rue des instituteurs")
                ->setStreetNumber(rand(1, 100));
            $teacher = (new Teacher)
                ->setAddress($address)
                ->setEmail("patrick.rof@mail.com")
                ->setFirstname("Patrick_{$subject->getName()}")
                ->setName("Rof")
                ->setTelephone("07 98 76 " . rand(10, 99) . " " . rand(10, 99))
                ->setRoles(["ROLE_TEACHER", "ROLE_STAFF"]);

            $teachingContract = (new TeachingContract)
                ->setSubject($subject)
                ->setTeacher($teacher);

            foreach ($schoolClasses as $class) {
                $teachingContract->addSchoolClass($class);
            }

            $manager->persist($teachingContract);
            $manager->flush();
        }
    }

    public function createStudentAndLegalResponsible(ObjectManager $manager, int $nbStudent = 120) : void
    {
        $languages = $manager->getRepository(Language::class)->findAll();
        $classes = $manager->getRepository(SchoolClass::class)->findAll();

        for ($i = 0; $i < $nbStudent; $i++){
            $address = (new Address)
                ->setPostalcode(rand(10000,99999))
                ->setTown("Eysier")
                ->setStreetName("rue des étudiants")
                ->setStreetNumber(rand(1, 100));

            $student = (new Student())
                ->setAddress($address)
                ->setIne(rand(1000, 9999))
                ->setEmail("student$i@mail.com")
                ->setFirstname("Student_$i")
                ->setName("tud")
                ->setTelephone("07 98 76 " . rand(10, 99) . " " . rand(10, 99));

            $legalResponsible = (new LegalResponsible())
                ->setAddress($address)
                ->setEmail("legalResponsible$i@mail.com")
                ->setFirstname("LegalResponsible_$i")
                ->setName("egalR")
                ->setTelephone("07 98 76 " . rand(10, 99) . " " . rand(10, 99));

            $lrs = (new LegalResponsibleStudent())
                ->setResponsibleNumber(1)
                ->setLegalresponsible($legalResponsible)
                ->setStudent($student);

            shuffle($languages);
            shuffle($classes);
            $class = $classes[0];

            $registration = (new Registration)
                ->setLanguage($languages[0])
                ->setYear((new \DateTime)->setDate(2022, 9, 1));

            $student->addRegistration($registration);

            while( (count($class->getRegistrations()) >= $class->getMaxRegistrations()) ){
                shuffle($classes);
                $class = $classes[0];
            }

            $registration->setSchoolClass($class);

            $manager->persist($legalResponsible);
            $manager->persist($lrs);
            $manager->persist($registration);
            $manager->persist($student);
            $manager->flush();
        }
    }
}
