<?php

namespace App\Controller;

use App\Entity\SchoolClasses\SchoolLevel;
use App\Form\SchoolLevelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/niveaux_scolaire', name: 'school_level')]
#[IsGranted('ROLE_SCOLARITE')]
class SchoolLevelController extends AbstractController
{

    #[Route('/ajouter', name:"_add")]
    public function addSchoolLevelAction(EntityManagerInterface $entityManager, Request $request) : Response
    {
        $schoolLevel = (new SchoolLevel());

        $form = $this->createForm(SchoolLevelType::class, $schoolLevel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $entityManager->persist($schoolLevel);
            $entityManager->flush();

            $this->addFlash('info', 'Le niveau a été ajouté');
            return $this->redirectToRoute('school_level_list');
        }

        return $this->render('school_level/form-schoolLevel.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/modifier-{schoolLevelId}-{route}', name:"_edit")]
    public function editSchoolLevelAction(string $route, int $schoolLevelId, EntityManagerInterface $entityManager, Request $request) : Response
    {
        $schoolLevel = $entityManager->getRepository(SchoolLevel::class)->find($schoolLevelId);

        $form = $this->createForm(SchoolLevelType::class, $schoolLevel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $entityManager->persist($schoolLevel);
            $entityManager->flush();

            $this->addFlash('info', 'Le niveau a été modifié');
            return $this->redirectToRoute($route);
        }

        return $this->render('school_level/form-schoolLevel.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/liste', name:"_list")]
    public function viewAllLevelAction(EntityManagerInterface $entityManager) : Response
    {
        $schoolLevel = $entityManager->getRepository(SchoolLevel::class)->findAll();

        return $this->render('school_level/viewAllSchoolLevel.html.twig', ['levels'=>$schoolLevel]);
    }
}
