<?php

namespace App\Controller\SubjectsController;

use App\Entity\Equipments\Equipment;
use App\Entity\Equipments\TypeEquipment;
use App\Form\EquipmentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/equipements', name: 'equipment')]
#[IsGranted('ROLE_DOCUMENTATION')]
class EquipmentController extends AbstractController
{
    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function listEquipmentAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {
//        $equipementRepository = $entityManager->getRepository(Equipment::class);
//        $equipments = $equipementRepository->findAll();
        $equipmentRepository = $entityManager->getRepository(Equipment::class);

        if(!($equipments = $equipmentRepository->findAllPage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas d'équipements à afficher");
        }

        $nbTotalPages = intval(ceil(count($equipments) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }


        return $this->render('equipment/ListEquipments.html.twig', ['equipments' => $equipments,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

    #[Route('/equipement-{id}', name: '_view_profile', requirements: ['id' => '\d+'])]
    public function viewProfileEquipmentAction(int $id, EntityManagerInterface $entityManager): Response
    {
        $equipementRepository = $entityManager->getRepository(Equipment::class);
        if (!($equipment = $equipementRepository->find($id))) {
            throw new NotFoundHttpException("Le matériel que vous cherchez n'existe pas ou est masqué.");
        }

        return $this->render('equipment/profileEquipment.html.twig', ['equipment' => $equipment]);
    }

    #[Route('/ajouter', name: '_add')]
    public function addEquipmentAction(EntityManagerInterface $entityManager, Request $request,): Response
    {

        $types = $entityManager->getRepository(TypeEquipment::class)->findAll();
        $sbis = array ();
        $tbis = array ();


        foreach($types as $value)
        {
            $tbis[$value->getType()] = $value;
        }

        $equipment = new Equipment();
        $form = $this->createForm(EquipmentType::class, $equipment);

        $form->add('type', ChoiceType::class,
            [
                'label' => 'Type',
                'choices'=>$tbis
            ]);
        $form->add('send', SubmitType::class, ['label' => 'Ajouter le matériel']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($equipment);

            try {
                $entityManager->flush();
                $this->addFlash('info', "Le matériel a été créé");
                return $this->redirectToRoute('equipment_view_all');
            }catch (\Exception $e){
                $this->addFlash('error', "L'année entrée est invalide elle doit etre comprise entre 2000 et " . date_format(new \DateTime, 'Y'));
                return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'equipment'=>'']);
            }
        }
        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'equipment'=>'']);
    }

    #[Route('/supprimer-{id}',name: '_delete', requirements: ['id' => '\d+'])]
    public function deleteEquipmentAction(EntityManagerInterface $entityManager, int $id) : Response
    {
        $equipementRepository = $entityManager->getRepository(Equipment::class);

        if(!($equipment = $equipementRepository->find($id)) ){
            throw new NotFoundHttpException("Ce materiel n'existe pas !");
        }

        $entityManager->remove($equipment);
        $entityManager->flush();

        $this->addFlash('info', "Le materiel " . $equipment->getTitle() . " a bien été supprimé.");
        return $this->redirectToRoute('equipment_view_all');
    }

    #[Route('/modifier-{id}-{route}', name: '_edit')]
    public function editEquipmentAction(int $id, string $route, EntityManagerInterface $entityManager, Request $request): Response
    {
        $types = $entityManager->getRepository(TypeEquipment::class)->findAll();
        $tbis = array ();

        foreach($types as $value)
        {
            $tbis[$value->getType()] = $value;
        }

        if(!($equipment = $entityManager->getRepository(Equipment::class)->find($id))){
            throw $this->createNotFoundException("Le materiel n'existe pas");
        }

        $form = $this->createForm(EquipmentType::class, $equipment);

        $form->add('type', ChoiceType::class,
            [
                'label' => 'Type',
                'choices'=>$tbis
            ]);
        $form->add('send', SubmitType::class, ['label' => 'Modifier le matériel']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($equipment);
            $entityManager->flush();

            $this->addFlash('info', "Le matériel a été modifié");

            return $this->redirectToRoute($route, ['id'=>$id]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'equipment'=>'']);
    }

}
