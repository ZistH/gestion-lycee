<?php

namespace App\Controller\SubjectsController;

use App\Entity\Equipments\TypeEquipment;
use App\Form\EquipmentType;
use App\Form\TypeEquipmentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('accueil/types_equipement', name: 'equipmentType')]
class  EquipmentTypeController extends AbstractController
{
    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function listEquipmentTypeAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {
//        $equipementRepository = $entityManager->getRepository(TypeEquipment::class);
//        $equipmentTypes = $equipementRepository->findAll();
        $equipmentTypesRepository = $entityManager->getRepository(TypeEquipment::class);

        if(!($equipmentTypes = $equipmentTypesRepository->findAllPage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas de type d'équipement à afficher");
        }

        $nbTotalPages = intval(ceil(count($equipmentTypes) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }

        return $this->render('equipmentType/ListEquipmentType.html.twig', ['equipmentTypes' => $equipmentTypes,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

    #[Route('/type-equipement-{id}', name: '_view_profile', requirements: ['id' => '\d+'])]
    public function profileEquipmentTypeAction(int $id, EntityManagerInterface $entityManager): Response
    {
        $equipementRepository = $entityManager->getRepository(TypeEquipment::class);
        if (!($equipmentType = $equipementRepository->find($id))) {
            throw new NotFoundHttpException("Le type de matériel que vous cherchez n'existe pas ou est masqué.");
        }

        return $this->render('equipmentType/profileEquipmentType.html.twig', ['equipmentType' => $equipmentType]);
    }
    #[Route('/ajouter', name: '_add')]
    public function addEquipmentTypeAction(EntityManagerInterface $entityManager, Request $request,): Response
    {

        $equipmentType = new TypeEquipment();
        $form = $this->createForm(TypeEquipmentType::class, $equipmentType);

        $form->add('send', SubmitType::class, ['label' => 'Ajouter le type de matériel']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($equipmentType);
            $entityManager->flush();
            $this->addFlash('info', "Le type de matériel a été créé");

            return $this->redirectToRoute('equipmentType_view_all');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'equipmentType'=>'']);
    }

    #[Route('/supprimer-{id}',name: '_delete', requirements: ['id' => '\d+'])]
    public function deleteEquipmentTypeAction(EntityManagerInterface $entityManager, int $id) : Response
    {
        $equipmentTypeRepository = $entityManager->getRepository(EquipmentType::class);

        if(!($equipmentType = $equipmentTypeRepository->find($id)) ){
            throw new NotFoundHttpException("Ce type de materiel n'existe pas !");
        }

        $entityManager->remove($equipmentType);
        $entityManager->flush();

        $this->addFlash('info', "Le type de materiel " . $equipmentType->getType() . " à bien été supprimé.");
        return $this->redirectToRoute('equipmentType_view_all');
    }

    #[Route('/modifier-{id}-{route}', name: '_edit')]
    public function editEquipmentTypeAction(int $id, string $route, EntityManagerInterface $entityManager, Request $request): Response
    {

        if(!($equipmentType = $entityManager->getRepository(TypeEquipment::class)->find($id))){
            throw $this->createNotFoundException("Le materiel n'existe pas");
        }

        $form = $this->createForm(TypeEquipmentType::class, $equipmentType);

        $form->add('send', SubmitType::class, ['label' => 'Modifier le type de matériel']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($equipmentType);
            $entityManager->flush();
            $this->addFlash('info', "Le type de matériel a été modifié");

            return $this->redirectToRoute($route, ['id'=>$id]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'equipmentType'=>'']);
    }
}
