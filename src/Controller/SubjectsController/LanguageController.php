<?php

namespace App\Controller\SubjectsController;

use App\Entity\Subjects\IsTaught;
use App\Entity\Subjects\Language;
use App\Form\IsTaughtType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route('accueil/matières/langues', name: "language")]
#[IsGranted('ROLE_SCOLARITE')]
class LanguageController extends AbstractController
{
    #[Route('/liste/{currentPage}', name: '_view_all',
        requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function viewAllLanguageAction(int $currentPage, EntityManagerInterface $entityManager) : Response
    {
        $languageRepository = $entityManager->getRepository(Language::class);
        $langues = $languageRepository->findAll();
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $areTaught = $isTaughtRepository->findAllAreTaughtLanguages($langues, $currentPage, $this->getParameter('nb_max_subject_per_page'));

        $nbTotalPages = intval(ceil(count($langues) / $this->getParameter('nb_max_subject_per_page')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw $this->createNotFoundException("numéro de page incorrect");
        }

        return $this->render('listView/ListLanguages.html.twig', [
            'areTaught' => $areTaught,
            "nbTotalPages"=> $nbTotalPages,
            "currentPage" => $currentPage
        ]);
    }

    #[Route('-{id}', name: '_view')]
    public function showSubjectAction(int $id, EntityManagerInterface $entityManager): Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $isTaught = $isTaughtRepository->find($id);

        return $this->render('profileView/SubjectView.html.twig', ['isTaught' => $isTaught, 'langue'=>'']);
    }

    #[Route('/ajouter', name: '_create')]
    public function create(EntityManagerInterface $entityManager, Request $request) : Response
    {
        $langue = (new Language);
        $isTaught = (new IsTaught)
            ->setSubject($langue);

        $form = $this->createForm(IsTaughtType::class, $isTaught)
            ->add('send', SubmitType::class, ['label' => 'Créer la langue'])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

//            foreach ($form->get('equipments')->getData() as $equipment){
//                $isTaught->addEquipment($equipment);
//            }

            if($langue = $entityManager->getRepository(Language::class)->findOneBy(['name'=>$langue->getName()])){
                $isTaught->setSubject($langue);
            }

            $entityManager->persist($isTaught);
            $entityManager->flush();

            $this->addFlash('info', "La langue a été créée");
            return $this->redirectToRoute('language_view_all');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'language' => $langue]);
    }

    #[Route('/modifier-{langueId}-{route}', name: '_edit', requirements: ['langueId' => '\d+'])]
    public function editLanguage(string $route, int $langueId, EntityManagerInterface $entityManager, Request $request): Response
    {
        $isTaught = $entityManager->getRepository(IsTaught::class)->find($langueId);

//        $schoolClasses = $isTaught->getSchoolLevel()->getSchoolClasses();
//        foreach ($schoolClasses as $class){
//            foreach ($class->getRegistrations() as $registration) {
//                foreach ($isTaught->getEquipments() as $equipment) {
//                    $registration->removeEquipment($equipment);
//                }
//            }
//        }

        $form = $this->createForm(IsTaughtType::class, $isTaught)
            ->add('send', SubmitType::class, ['label' => 'Modifier la langue'])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

//            foreach ($form->get('equipments')->getData() as $equipment){
//                $isTaught->addEquipment($equipment);
//            }

            $entityManager->persist($isTaught);
            $entityManager->flush();

            $this->addFlash('info', "La langue \"{$isTaught->getSubject()}\" a bien été modifié pour le niveau de {$isTaught->getSchoolLevel()}.");
            return $this->redirectToRoute($route, ['id'=>$isTaught->getId()]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'language' => '']);

    }

    #[Route('/supprimer-{id}-{route}', name: '_delete', requirements: ['id' => '\d+'])]
    public function deleteLangueAction(int $id, string $route , EntityManagerInterface $entityManager): Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $isTaught = $isTaughtRepository->find($id);

        if (!($isTaught)) {
            throw $this->createNotFoundException("Cette matière n'existe pas !");
        }

        if(count($isTaughtRepository->findBy(['subject'=>$isTaught->getSubject()])) == 1) {
            $entityManager->remove($isTaught->getSubject());
        }

        $entityManager->remove($isTaught);
        $entityManager->flush();

        $this->addFlash('info', "La langue \"{$isTaught->getSubject()}\" a bien été supprimée pour le niveau de {$isTaught->getSchoolLevel()}.");
        return $this->redirectToRoute($route);
    }
}
