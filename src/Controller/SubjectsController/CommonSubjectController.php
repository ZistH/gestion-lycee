<?php

namespace App\Controller\SubjectsController;

use App\Entity\Subjects\CommonSubject;
use App\Entity\Subjects\IsTaught;
use App\Form\IsTaughtType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class contenant toutes les routes et méthodes permettant les interactions
 * avec les pages liées aux matières du tronc commun.
 */
#[Route('accueil/matières', name: "subject")]
#[IsGranted('ROLE_SCOLARITE')]
class CommonSubjectController extends AbstractController
{
    /**
     * Liste la totalité des matières enseignée dans la table "IsTaught"
     * @param int $currentPage
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/liste/{currentPage}', name: '_view_all',
        requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function viewAllSubjectAction(int $currentPage, EntityManagerInterface $entityManager) : Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);

        if(!($isTaughtWithPaging = $isTaughtRepository->findAllIsTaughtWithPaging($currentPage, $this->getParameter('nb_max_subject_per_page')))) {
            throw $this->createNotFoundException("pas de matière à afficher");
        }

        $nbTotalPages = intval(ceil(count($isTaughtWithPaging) / $this->getParameter('nb_max_subject_per_page')));
        if($nbTotalPages == 0) {
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages) {
            throw $this->createNotFoundException("numéro de page incorrect");
        }

        return $this->render('listView/ListMatieres.html.twig', [
            'areTaught' => $isTaughtWithPaging,
            "nbTotalPages"=> $nbTotalPages,
            "currentPage" => $currentPage
        ]);
    }

    #[Route('/matière-{id}', name: '_view')]
    public function showSubjectAction(int $id, EntityManagerInterface $entityManager) : Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $isTaught = $isTaughtRepository->find($id);

        return $this->render('profileView/SubjectView.html.twig', ['isTaught' => $isTaught]);
    }

    #[Route('/ajouter', name: '_create')]
    public function create(EntityManagerInterface $entityManager, Request $request) : Response
    {
        $subject = (new CommonSubject());
        $isTaught = (new IsTaught())
            ->setSubject($subject);

        $form = ($this->createForm(IsTaughtType::class, $isTaught))
            ->add('send', SubmitType::class, ['label' => 'Créer la matière'])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($form->get('equipments')->getData() as $equipment){
                $isTaught->addEquipment($equipment);
            }

            if($subject = $entityManager->getRepository(CommonSubject::class)->findOneBy(['name'=>$subject->getName()])){
                $isTaught->setSubject($subject);
            }

            $entityManager->persist($isTaught);
            $entityManager->flush();

            $this->addFlash('info', "La nouvelles matière enseigné a bien été créée");
            return $this->redirectToRoute('subject_view_all');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'commonSubject' => '']);
    }

    #[Route('/supprimer-{id}-{route}', name: '_delete', requirements: ['id' => '\d+'])]
    public function deleteMatiere(int $id, string $route, EntityManagerInterface $entityManager) : Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);

        if (!($isTaught = $isTaughtRepository->find($id))) {
            throw $this->createNotFoundException("Cette matière n'existe pas !");
        }

        if(count($isTaughtRepository->findBy(['subject'=>$isTaught->getSubject()])) == 1) {
            $entityManager->remove($isTaught->getSubject());
        }

        $entityManager->remove($isTaught);
        $entityManager->flush();

        $this->addFlash('info', "La matière \"{$isTaught->getSubject()}\" a bien été supprimée pour le niveau de {$isTaught->getSchoolLevel()}.");
        return $this->redirectToRoute($route);
    }

    #[Route('/modifier-{isTaughtId}-{route}', name: '_edit', requirements: ['id' => '\d+'])]
    public function editCommonSubject(string $route, int $isTaughtId, EntityManagerInterface $entityManager, Request $request) : Response
    {
        $isTaught = $entityManager->getRepository(IsTaught::class)->find($isTaughtId);

        $schoolClasses = $isTaught->getSchoolLevel()->getSchoolClasses();
        foreach ($schoolClasses as $class){
            foreach ($class->getRegistrations() as $registration) {
                foreach ($isTaught->getEquipments() as $equipment) {
                    $registration->removeEquipment($equipment);
                }
            }
        }

        $form = $this->createForm(IsTaughtType::class, $isTaught)
            ->add('send', SubmitType::class, ['label' => 'Modifier la matière'])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($form->get('equipments')->getData() as $equipment){
                $isTaught->addEquipment($equipment);
            }

            $entityManager->persist($isTaught);
            $entityManager->flush();

            $this->addFlash('info', "La matière \"{$isTaught->getSubject()}\" a bien été supprimée pour le niveau de {$isTaught->getSchoolLevel()}.");
            return $this->redirectToRoute($route, ['id'=>$isTaughtId]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'commonSubject' => '']);
    }
}
