<?php

namespace App\Controller\SubjectsController;

use App\Entity\Subjects\IsTaught;
use App\Entity\Subjects\OptionalSubject;
use App\Form\IsTaughtType;
use App\Form\OptionalSubjectType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


#[Route('accueil/matieres/facultatives', name: "optional_subject")]
#[IsGranted('ROLE_SCOLARITE')]
class OptionalSubjectController extends AbstractController
{
    #[Route('/liste-{currentPage}', name: '_view_all',
        requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function viewAllOptionalSubjectAction(int $currentPage, EntityManagerInterface $entityManager) : Response
    {
        $optionalSubjectRepository = $entityManager->getRepository(OptionalSubject::class);
        $optionalSubjects = $optionalSubjectRepository->findAll();
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $areTaughtLanguages = $isTaughtRepository->findAllAreTaughtForOptionalSubject($optionalSubjects, $currentPage, $this->getParameter('nb_max_subject_per_page'));
        dump($areTaughtLanguages->getQuery());
        dump($areTaughtLanguages);
        $nbTotalPages = intval(ceil(count($optionalSubjects) / $this->getParameter('nb_max_subject_per_page')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw $this->createNotFoundException("numéro de page incorrect");
        }

        return $this->render('listView/ListOptionalSubject.html.twig', [
            'areTaught' => $areTaughtLanguages,
            'nbTotalPages'=> $nbTotalPages,
            'currentPage' => $currentPage
        ]);

    }

    #[Route('-{id}', name: '_view')]
    public function showSubjectAction(int $id, EntityManagerInterface $entityManager): Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $isTaught = $isTaughtRepository->find($id);

        return $this->render('profileView/SubjectView.html.twig', ['isTaught' => $isTaught, 'langue'=>'']);

    }

    #[Route('/ajouter', name: '_create')]
    public function create(EntityManagerInterface $entityManager, Request $request): Response
    {
        $optionalSubject = (new OptionalSubject())
            ->setDisabled(false);
        $isTaught = (new IsTaught())
            ->setSubject($optionalSubject);

        $form = $this->createForm(IsTaughtType::class, $isTaught);


        $form->add('send', SubmitType::class, ['label' => 'Créer la matière facultative']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($form->get('equipments')->getData() as $equipment){
                $isTaught->addEquipment($equipment);
            }

            if($optionalSubject = $entityManager->getRepository(OptionalSubject::class)->findOneBy(['name'=>$optionalSubject->getName()])){
                $isTaught->setSubject($optionalSubject);
            }

            $entityManager->persist($isTaught);
            $entityManager->flush();

            $this->addFlash('info', "La matière facultative {$isTaught->getSubject()} a été créée");
            return $this->redirectToRoute('optional_subject_view_all');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'optionalSubject' => '']);
    }


    #[Route('/modifier-{optionalSubjectId}-{route}', name: '_edit', requirements: ['optionalSubjectId' => '\d+'])]
    public function editLanguage(string $route, int $optionalSubjectId, EntityManagerInterface $entityManager, Request $request): Response
    {
        $isTaught = $entityManager->getRepository(IsTaught::class)->find($optionalSubjectId);

        $form = $this->createForm(IsTaughtType::class, $isTaught)
            ->add('send', SubmitType::class, ['label' => 'Modifier la matière facultative'])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($form->get('equipments')->getData() as $equipment){
                $isTaught->addEquipment($equipment);
            }

            $entityManager->persist($isTaught);
            $entityManager->flush();

            $this->addFlash('info', "La matière facultative \"{$isTaught->getSubject()}\" a bien été modifiée pour le niveau de {$isTaught->getSchoolLevel()}.");
            return $this->redirectToRoute($route);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'optionalSubject' => $isTaught]);

    }

    #[Route('/supprimer-{id}-{route}', name: '_delete', requirements: ['id' => '\d+'])]
    public function deleteLangueAction(int $id, string $route, EntityManagerInterface $entityManager): Response
    {
        $isTaughtRepository = $entityManager->getRepository(IsTaught::class);
        $isTaught = $isTaughtRepository->find($id);

        if (!($isTaught)) {
            throw $this->createNotFoundException("Cette matière n'existe pas !");
        }

        if(count($isTaughtRepository->findBy(['subject'=>$isTaught->getSubject()])) == 1) {
            $entityManager->remove($isTaught->getSubject());
        }

        $entityManager->remove($isTaught);
        $entityManager->flush();

        $this->addFlash('info', "La matière \"{$isTaught->getSubject()}\" a bien été supprimée pour le niveau de {$isTaught->getSchoolLevel()}.");
        return $this->redirectToRoute($route);
    }
}
