<?php

namespace App\Controller;

use App\Entity\Users\Students\Registration;
use App\Entity\Users\Students\Student;
use App\Entity\Users\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/recherche', name: 'app_search')]
class SearchController extends AbstractController
{
    #[Route('/utilisateurs', name: '_user')]
    #[IsGranted('ROLE_RH')]
    public function SearchUserAction(EntityManagerInterface $entityManager): Response
    {
        $users = $entityManager->getRepository(User::class)->findAll();

        return $this->render('search/searchUser.html.twig', ['users'=>$users]);
    }

    #[Route('/étudiants', name: '_student')]
    #[IsGranted('ROLE_SCOLARITE')]
    public function SearchStudentAction(EntityManagerInterface $entityManager): Response
    {
        $students = $entityManager->getRepository(Student::class)->findAll();

        return $this->render('search/search.html.twig', ['students'=>$students]);
    }
}
