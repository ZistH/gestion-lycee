<?php

namespace App\Controller;

use App\Entity\Users\Staff\Staff;
use App\Form\StaffType;
use App\Service\GenerateLoginAndPassword;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class AccueilController extends AbstractController
{
    #[Route('/accueil', name: 'accueil')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function acceuilAction(EntityManagerInterface $entityManager, Security $security): Response
    {
        return $this->render('accueil.html.twig');
    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/', name: 'create_rh')]
    public function createRHAction(GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $userRepository = $entityManager->getRepository(Staff::class);
        if ($userRepository->findRH()) {
            return $this->redirectToRoute('accueil');
        }

        $rh = (new Staff())
            ->setRoles(["ROLE_RH", "ROLE_STAFF"])
            ->setDisabled(false);

        $form = $this->createForm(StaffType::class, $rh);
        $form->add('send', SubmitType::class, ['label' => 'Créer le RH']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $generateLoginAndPassword->generate($rh);

//            $hashedPassword = $passwordHasher->hashPassword($rh, $rh->getPassword());
//            $rh->setPassword($hashedPassword);

            $entityManager->persist($rh);
            $entityManager->flush();

            $this->addFlash('info', "Le RH a été créé");
            return $this->redirectToRoute('accueil');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'staff' => '']);
    }

    #[Route('/equipements', name: '_add_equipment')]
    #[IsGranted('ROLE_DOCUMENTATION')]
    public function addEquipmentAction(): Response
    {
        return $this->render('addButtonPage/addEquipment.html.twig');
    }

    #[Route('/matieres', name: '_add_subject')]
    #[IsGranted('ROLE_SCOLARITE')]
    public function addSubjectAction(): Response
    {
        return $this->render('addButtonPage/addMatiere.html.twig');
    }

    #[Route('/classes', name: '_add_group')]
    #[IsGranted('ROLE_SCOLARITE')]
    public function addGroupAction(): Response
    {
        return $this->render('addButtonPage/addSchoolGroup.html.twig');
    }

    #[Route('/personnel', name: '_add_staff')]
    #[IsGranted('ROLE_RH')]
    public function addClassAction(): Response
    {
        return $this->render('addButtonPage/addStaff.html.twig');
    }
}
