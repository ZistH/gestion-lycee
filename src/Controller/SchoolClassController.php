<?php

namespace App\Controller;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Users\Students\Student;
use App\Form\SchoolClassType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/classes', name: 'school_class')]
class SchoolClassController extends AbstractController
{
    #[Route('/liste/{currentPage}', name: '_list',
        requirements: ['currentPage'=>'\d+'], defaults: ['currentPage'=>1])]
//    #[IsGranted("ROLE_SCOLARITE or ROLE_DOCUMENTATION")] CF: security.yaml
    public function listSchoolClassAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {
        if(!($classes = $entityManager->getRepository(SchoolClass::class)->findAllWithPagination($currentPage, $this->getParameter('nb_classes_per_page')))){
            throw $this->createNotFoundException('Aucune classe à afficher');
        }

        $nbPage = intval(ceil(count($classes) / $this->getParameter('nb_classes_per_page')));


        return $this->render('school_class/schoolClassList.html.twig', [
            'classes' => $classes,
            'currentPage' => $currentPage,
            'nbTotalPages' => $nbPage
        ]);
    }

    #[Route('/ajouter', name: '_add')]
    #[IsGranted('ROLE_SCOLARITE')]
    public function addSchoolClassAction(EntityManagerInterface $entityManager, Request $request): Response
    {

        $class = (new SchoolClass());

        $form = $this->createForm(SchoolClassType::class, $class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $entityManager->persist($class);
            $entityManager->flush();
            $this->addFlash('info', "La classe a eté créée");

            return $this->redirectToRoute('school_class_list');

        }

        return $this->render('school_class/add-schoolClass.html.twig', ['form'=> $form->createView()]);
    }

    #[Route('/modifier-{classId}-{route}', name: "_edit")]
    #[IsGranted('ROLE_SCOLARITE')]
    public function editSchoolClassAction(int $classId, string $route, EntityManagerInterface $entityManager, Request $request): Response
    {

        if(!($class = $entityManager->getRepository(SchoolClass::class)->find($classId))){
            throw $this->createNotFoundException('pas de classe à afficher');
        }

        $form = $this->createForm(SchoolClassType::class, $class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $entityManager->flush();
            $this->addFlash('info', "La classe a eté modifiée");

            return $this->redirectToRoute($route, ['classId' => $classId]);
        }

        return $this->render('school_class/add-schoolClass.html.twig', ['form'=> $form->createView()]);
    }

    #[Route('/liste-eleves-{classId}', name:"_show_student")]
    #[IsGranted('ROLE_STAFF')]
    public function showStudentAction(int $classId, EntityManagerInterface $entityManager) : Response
    {
        $schoolClass = $entityManager->getRepository(SchoolClass::class)->find($classId);
        $students = $entityManager->getRepository(Student::class)->findBySchoolCLassAndYear($schoolClass, date_format(new \DateTime, "Y"));

        return $this->render('/listView/ListUsers.html.twig', ['users'=>$students, 'className'=>$schoolClass->getName()]);
    }

    #[Route('/classe-{classId}', name:"_show")]
    #[IsGranted('ROLE_STAFF')]
    public function showSchoolClassAction(int $classId, EntityManagerInterface $entityManager) : Response
    {
        $schoolClass = $entityManager->getRepository(SchoolClass::class)->findWithTeachingContractByYearAndId($classId, date_format(new \DateTime, "Y"));
        $students = $entityManager->getRepository(Student::class)->findBySchoolCLassAndYear($schoolClass, date_format(new \DateTime, "Y"));

        return $this->render('school_class/schoolClassView.html.twig', ['students'=>$students, 'class'=>$schoolClass, 'contracts'=>$schoolClass->getContract()]);
    }
}
