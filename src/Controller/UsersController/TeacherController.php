<?php

namespace App\Controller\UsersController;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Users\Staff\Teacher;
use App\Entity\Users\Staff\TeachingContract;
use App\Form\Teacher\EditTeachingContractType;
use App\Service\GenerateLoginAndPassword;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/enseignants', name: 'teacher')]
class TeacherController extends AbstractController
{

    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    #[IsGranted('ROLE_RH')]
    public function viewAllAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {


        $teacherRepository = $entityManager->getRepository(Teacher::class);
        //$users = $staffRepository->findAll();


        if(!($users = $teacherRepository->findAllPage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas de professeur à afficher");
        }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }
        //$teacherRepository = $entityManager->getRepository(Teacher::class);
        //$users = $teacherRepository->findAll();

        $teachingContractRepository = $entityManager->getRepository(TeachingContract::class);
        $teachingContracts = $teachingContractRepository->findAll();

        return $this->render('listView/ListUsers.html.twig', ['users' => $users,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage, 'teachingContracts'=>$teachingContracts]);
    }

    #[Route('/ajouter', name: '_add')]
    #[IsGranted('ROLE_RH')]
    public function createTeacher(GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $teacher = (new Teacher())
            ->setDisabled(false)
            ->setRoles(["ROLE_TEACHER", "ROLE_STAFF"]);

        $teachingContract = (new TeachingContract)
            ->setTeacher($teacher);

        $form = $this->createForm(EditTeachingContractType::class, $teachingContract);
        $form->add('send', SubmitType::class, ['label' => 'Ajouter un professeur']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $generateLoginAndPassword->generate($teacher);

//            $hashedPassword = $passwordHasher->hashPassword($teacher, $teacher->getPassword());
//            $teacher->setPassword($hashedPassword);
            $entityManager->persist($teacher);
            $entityManager->persist($teachingContract);
            $entityManager->flush();

            $this->addFlash('info', "Le professeur a été créé");
            return $this->redirectToRoute('teacher_view_all');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'teacher'=>'']);
    }

    #[Route('/classes-{teacherid}/{currentPage}', name: '_voir_classes',
    requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    #[IsGranted('ROLE_TEACHER')]
    public function voirClassesAction(int $currentPage, int $teacherid, EntityManagerInterface $entityManager): Response
    {
        $teacher=$entityManager->getRepository(Teacher::class)->find($teacherid);
        $contratRepository = $entityManager->getRepository(TeachingContract::Class);
        $contrat = $contratRepository->findByTeacherAndYear($teacher,date_format(new \DateTime, "Y"));

        $classes = $entityManager->getRepository(SchoolClass::class)
            ->findByContractWithPaging($contrat, $currentPage, $this->getParameter('nb_classes_per_page'));

        $nbPage = intval(ceil(count($classes) / $this->getParameter('nb_classes_per_page')));

        return $this->render('school_class/schoolClassList.html.twig', [
            'classes' => $classes,
            'currentPage' => $currentPage,
            'nbTotalPages' => $nbPage
        ]);
    }

    #[Route('/renouveler_contrat-{teacherId}', name: '_renew_contract')]
    #[IsGranted('ROLE_RH')]
    public function renewContractAction(int $teacherId, EntityManagerInterface $entityManager, Request $request) : Response
    {
        $teacher = $entityManager->getRepository(Teacher::class)->find($teacherId);
//        $contract = $entityManager->getRepository(TeachingContract::class)->findByTeacherAndYear($teacher, (intval(date_format(new \DateTime(), "Y"))-1));

        $teachingContract = (new TeachingContract)
            ->setTeacher($teacher);
//            ->setSubject($contract->getSubject());

        $form = $this->createForm(EditTeachingContractType::class, $teachingContract);
        $form->add('send', SubmitType::class, ['label' => 'Renouveler le contrat']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

//            $generateLoginAndPassword->generate($teacher);

//            $hashedPassword = $passwordHasher->hashPassword($teacher, $teacher->getPassword());
//            $teacher->setPassword($hashedPassword);
            $entityManager->persist($teachingContract);
            $entityManager->persist($teacher);
            try{
                $entityManager->flush();
                $this->addFlash('info', "Le contrat a bien été renouvelé");
                return $this->redirectToRoute('teacher_view_all');
            }catch(\Exception $e){
                $this->addFlash('error', "Ce professeur est déjà titulaire d'un contrat pour cette année.");
                return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'teacher'=>'']);
            }
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'teacher'=>'']);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    #[Route('/modifier-{teacherId}-{route}', name: '_edit')]
    #[IsGranted('ROLE_RH')]
    public function editContractAction(int $teacherId, string $route, EntityManagerInterface $entityManager, Request $request) : Response
    {
        $teacher = $entityManager->getRepository(Teacher::class)->findById($teacherId);
        $contract = $entityManager->getRepository(TeachingContract::class)->findByTeacherAndYear($teacher, (intval(date_format(new \DateTime(), "Y"))));

        $form = $this->createForm(EditTeachingContractType::class, $contract);
        $form->add('send', SubmitType::class, ['label' => 'Modifier le professeur']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

//            $generateLoginAndPassword->generate($teacher);

//            $hashedPassword = $passwordHasher->hashPassword($teacher, $teacher->getPassword());
//            $teacher->setPassword($hashedPassword);
            $entityManager->persist($contract);
            $entityManager->persist($teacher);
            $entityManager->flush();

            $this->addFlash('info', "Le professeur a été modifié");
            return $this->redirectToRoute($route, ['login'=>$teacher->getUserIdentifier()]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'teacher'=>'']);
    }

}