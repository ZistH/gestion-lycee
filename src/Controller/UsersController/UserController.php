<?php

namespace App\Controller\UsersController;

use App\Entity\Users\Students\Registration;
use App\Entity\Users\Students\Student;
use App\Entity\Users\Staff\TeachingContract;
use App\Entity\Users\User;
use App\Form\LegalResponsible\LegalResponsibleType;
use App\Form\PasswordType;
use App\Form\StaffType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/utilisateurs', name: "user")]
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class UserController extends AbstractController
{

    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function listUserAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {

        $userRepository = $entityManager->getRepository(User::class);
        //$users = $staffRepository->findAll();

        if(!($users = $userRepository->findAllPage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas d'utilisateurs à afficher");
        }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));


        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }

        $teachingContractRepository = $entityManager->getRepository(TeachingContract::class);
        $teachingContracts = $teachingContractRepository->findAll();


        return $this->render('listView/ListUsers.html.twig', ['users' => $users, "nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage, 'teachingContracts'=>$teachingContracts]);
    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/profil-{login}', name: '_view_profile')]
    public function viewProfileUserAction(string $login, EntityManagerInterface $entityManager): Response
    {
        $userRepository = $entityManager->getRepository(User::class);
        if (!($user = $userRepository->myFindByLogin($login))) {
            throw new NotFoundHttpException("L'utilisateur que vous cherchez n'existe pas ou est masqué.");
        }

        $inscription = $user instanceof Student ? $user->getCurrentRegistration() : null;

        $teachingContractRepository = $entityManager->getRepository(TeachingContract::class);
        $contract = $teachingContractRepository->findOneBy(['teacher'=>$user,'date'=> date_format(new \DateTime, "Y")]);

        if($user === $this->getUser()){
            return $this->redirectToRoute('user_show_profile');
        }


        return $this->render('profileView/profileUser.html.twig', ['user' => $user, 'inscription' => $inscription, 'contract'=>$contract]);

    }

//    #[Route('/supprimer/{id}',name: '_delete', requirements: ['id' => '\d+'])]
//    #[IsGranted('ROLE_RH')]
//    public function deleteUserAction(EntityManagerInterface $entityManager, int $id) : Response
//    {
//        $staffRepository = $entityManager->getRepository(User::class);
//
//        if(!($staff = $staffRepository->find($id)) ){
//            throw new NotFoundHttpException("Cet utilisateur n'existe pas !");
//        }
//
//        $entityManager->remove($staff);
//        $entityManager->flush();
//
//        $this->addFlash('info', "Le compte " . $staff->getLogin() . " à bien été supprimé.");
//        return $this->redirectToRoute('user_view_all');
//    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/désactiver-{login}-{route}',name: '_disable')]
    #[IsGranted('ROLE_RH')]
    public function disableUserAction(string $login, string $route ,EntityManagerInterface $entityManager) : Response
    {
        $staffRepository = $entityManager->getRepository(User::class);
        if(!($staff = $staffRepository->myFindByLogin($login))){
            throw $this->createNotFoundException("Cet utilisateur n'existe pas !");
        }

        $staff->setDisabled(true);
        $entityManager->persist($staff);
        $entityManager->flush();
        $this->addFlash('info', "Le compte a été désactivé");

        return $this->redirectToRoute($route);
    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/activer-{login}-{route}',name: '_enable')]
    #[IsGranted('ROLE_RH')]
    public function enableUserAction(string $login, string $route, EntityManagerInterface $entityManager) : Response
    {
        $staffRepository = $entityManager->getRepository(User::class);

        if(!($staff = $staffRepository->myFindByLogin($login))){
            throw $this->createNotFoundException("Cet utilisateur n'existe pas !");
        }

        $staff->setDisabled(false);
        $entityManager->persist($staff);
        $entityManager->flush();

        $this->addFlash('info', "Le compte a été activé");
        return $this->redirectToRoute($route);
    }

    #[Route('/modifier-mon-profil',name: '_edit_self')]
    public function userSelfEdit(EntityManagerInterface $entityManager,Request $request) : Response
    {

        if($this->isGranted('ROLE_LEGAL_RESPONSIBLE')){
            $form = $this->createForm(LegalResponsibleType::class, $this->getUser());
            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $entityManager->flush();

                $this->addFlash('info', "Le profil a été édité");
                return $this->redirectToRoute('user_show_profile');
            }
            return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'legalResponsible'=>'']);
        }

        if($this->isGranted('ROLE_TEACHER')){
            $form = $this->createForm(StaffType::class, $this->getUser());
            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $entityManager->flush();

                $this->addFlash('info', "Le profil a été édité");
                return $this->redirectToRoute('user_show_profile');
            }
            return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'teacher'=>'']);
        }

        if($this->isGranted('ROLE_RH')){
            $form = $this->createForm(StaffType::class, $this->getUser());
            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $entityManager->flush();

                $this->addFlash('info', "Le profil a été édité");
                return $this->redirectToRoute('user_show_profile');
            }
            return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'rh'=>'']);
        }

        throw $this->createNotFoundException('Page inexistante');

//        $user = $this->getUser();
//
//        if($user->getRoles()[0]=="ROLE_STUDENT"){
//            $form = $this->createForm(StudentFromLegalResponsibleType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff'=>'']);
//
//        }
//        else if($user->getRoles()[0]=="ROLE_STAFF") {
//
//            $form = $this->createForm(StaffType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff' => '']);
//        }
//        else if($user->getRoles()[0]=="ROLE_TEACHER") {
//
//            $form = $this->createForm(StaffType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff' => '']);
//        }
//        else if($user->getRoles()[0]=="ROLE_SCOLARITE") {
//
//            $form = $this->createForm(StaffType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff' => '']);
//        }
//        else if($user->getRoles()[0]=="ROLE_DOCUMENTATION") {
//
//            $form = $this->createForm(StaffType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff' => '']);
//        }
//        else if($user->getRoles()[0]=="ROLE_RH") {
//
//            $form = $this->createForm(StaffType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff' => '']);
//        }
//        else{
//            $form = $this->createForm(LegalResponsibleType::class, $user);
//            $form->add('send', SubmitType::class, ['label' => 'Editer le compte']);
//            $form->handleRequest($request);
//
//            if ($form->isSubmitted() && $form->isValid()) {
//
////            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
////            $staff->setPassword($hashedPassword);
//                $entityManager->persist($user);
//                $entityManager->flush();
//
//                $this->addFlash('info', "Le profil a été edité");
//                return $this->redirectToRoute('user_show_profile');
//            }
//            return $this->render('Forms/formsView.html.twig', ['form' => $form->createView(), 'staff'=>'']);
//        }
    }

    #[Route('/mon-profil',name: '_show_profile')]
    public function userProfile(EntityManagerInterface $entityManager) : Response
    {
        $user = $this->getUser();
        if($this->isGranted('ROLE_STUDENT') and $user instanceof Student) {
            $registrationRepository = $entityManager->getRepository(Registration::class);
            $inscription = $registrationRepository->findOneBy(['student'=>$this->getUser(),'year'=> date_format(new \DateTime, "Y")]);

            return $this->render('profileView/profileUser.html.twig', ['user' => $user,'edit'=>'', 'inscription'=>$inscription]);
        }

        $teachingContract = $entityManager->getRepository(TeachingContract::class);
        $contract = $teachingContract->findOneBy(['teacher'=>$this->getUser(),'date'=> date_format(new \DateTime, "Y")]);

        return $this->render('profileView/profileUser.html.twig', ['user' => $user,'edit'=>'', 'contract'=>$contract]);

    }

    #[Route('/edit/password/{login}',name: '_edit_password')]
    public function userEditPassword(string $login,EntityManagerInterface $entityManager,Request $request) : Response
    {
        $user = $this->getUser();
        if(!$user->getUserIdentifier() == $login)
        {
            throw $this->createNotFoundException("mauvais compte");
        }
        $user = $entityManager->getRepository(User::class)->findOneBy(['login'=> $login]);
        $form = $this->createForm(PasswordType::class);
        $form->add('send', SubmitType::class, ['label' => 'modifier le mot de passe']);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($user->getPassword() != $form->get('password')->getData()){
                $this->addFlash('error', "Ancien mot de passe est incorrect");
                return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'editMdp'=>'']);
            }
            if($user->getPassword() == $form->get('newPassword')->getData()){
                $this->addFlash('error', "Les mots de passes sont identiques");
                return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'editMdp'=>'']);
            }
            if($form->get('newPassword')->getData() != $form->get('newPassword2')->getData()){
                $this->addFlash('error', "Les nouveaux mots de passes sont différents");
                return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'editMdp'=>'']);
            }
            $user->setPassword($form->get('newPassword')->getData());
            $entityManager->flush();

            $this->addFlash('info', "Le mot de passe a été édité");
            return $this->redirectToRoute('accueil');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'editMdp'=>'']);
    }
}
