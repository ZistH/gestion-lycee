<?php

namespace App\Controller\UsersController;

use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\User;
use App\Form\LegalResponsible\LegalResponsibleEditType;
use App\Form\LegalResponsible\LegalResponsibleType;
use App\Service\GenerateLoginAndPassword;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/responsables_legaux', name: 'legal_responsible')]
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class LegalResponsibleController extends AbstractController
{

    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function viewAllLegalResponsibleAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {
        $legalResponsibleRepository = $entityManager->getRepository(LegalResponsible::class);

        if(!($users = $legalResponsibleRepository->findAllPage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas de responsables légaux à afficher");
        }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }


        return $this->render('listView/ListUsers.html.twig', ['users' => $users,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

//    #[Route('/view/all/active', name: '_view_all_active')]
//    public function viewAllActiveStudentAction(EntityManagerInterface $entityManager): Response
//    {
//        $legalResponsibleRepository = $entityManager->getRepository(User::class);
//        $legalResponsible = $legalResponsibleRepository->findByRole('ROLE_LEGAL_RESPONSIBLE');
//
//        return $this->render('listView/ListUsers.html.twig', ['users'=>$legalResponsible]);
//    }
//
    #[Route('/ajouter', name: '_add')]
    public function addLegalResponsibleAction(GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $legalResponsible = (new LegalResponsible())
            ->setRoles(['ROLE_LEGAL_RESPONSIBLE'])
            ->setDisabled(false);

        $form = $this->createForm(LegalResponsibleType::class, $legalResponsible);
        $form->add('send', SubmitType::class, ['label' => 'Ajouter le responsable légal']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $generateLoginAndPassword->generate($legalResponsible);
            $entityManager->persist($legalResponsible);
            $entityManager->flush();
            $this->addFlash('info', "Le responsable légal a été créé");

            return $this->redirectToRoute('legal_responsible_view_all');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'legalResponsible'=>'']);
    }



    #[Route('/désactiver-{id}',name: '_disable', requirements: ['id' => '\d+'])]
    #[IsGranted('ROLE_SCOLARITE')]
    public function disableUserAction(int $id, EntityManagerInterface $entityManager) : Response
    {
        $staffRepository = $entityManager->getRepository(User::class);
        if(!($staff = $staffRepository->find($id))){
            throw new NotFoundHttpException("Ce responsable légal n'existe pas.");
        }

        $staff->setDisabled(true);
        $entityManager->persist($staff);
        $entityManager->flush();
        $this->addFlash('info', "L'utilisateur a été désactivé");

        return $this->redirectToRoute('legal_responsible_view_all');
    }

    #[Route('/activer-{id}',name: '_enable', requirements: ['id' => '\d+'])]
    #[IsGranted('ROLE_SCOLARITE')]
    public function enableUserAction(int $id, EntityManagerInterface $entityManager) : Response
    {
        $staffRepository = $entityManager->getRepository(User::class);

        if(!($staff = $staffRepository->find($id))){
            throw new NotFoundHttpException("Ce responsable légal n'existe pas.");
        }

        $staff->setDisabled(false);
        $entityManager->persist($staff);
        $entityManager->flush();

//la redirection sur la page d'origine est à faire
//        $currentRoute = $request->attributes->get('_route');
        $this->addFlash('info', "L'utilisateur a été activé");

        return $this->redirectToRoute('legal_responsible_view_all');
    }

    #[Route("/modifier-{idLegalResponsible}-{route}", name: "_scolarite_modifications")]
    public function studentScolariteModificationsAction(int $idLegalResponsible, string $route, EntityManagerInterface $entityManager, Request $request) : Response
    {
        if(!($legalResponsible = $entityManager->getRepository(LegalResponsible::class)->find($idLegalResponsible))){
            throw $this->createNotFoundException("Le responsable n'existe pas");
        }

        $form = $this->createForm(LegalResponsibleEditType::class, $legalResponsible);
        $form->add('send', SubmitType::class, ['label' => 'Valider les modifications.']);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $entityManager->flush();
            $this->addFlash('info', "La modification a été prise en compte");

            return $this->redirectToRoute($route, ['login'=> $legalResponsible->getUserIdentifier()]);
        }
        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'legalResponsible'=>'']);
    }

//    #[Route('/add/with/registration/{registrationId}', name: '_add_with_registration')]
//    public function addLegalResponsibleWithRegistrationAction(int $registrationId, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher) : Response
//    {
//        $legalResponsible = (new User())
//            ->setRoles(['ROLE_LEGAL_RESPONSIBLE'])
//            ->setDisabled(false);
//
//        $registrationRepository = $entityManager->getRepository(Registration::class);
//        $registration = $registrationRepository->find($registrationId);
//
//        $form = $this->createForm(LegalResponsibleType::class, $legalResponsible);
//        $form->add('send', SubmitType::class, ['label'=> 'Ajouter le responsable légal']);
//        $form->handleRequest($request);
//
//        if($form->isSubmitted() && $form->isValid()) {
//
//            $legalResponsibleRepository = $entityManager->getRepository(User::class);
//            if($oldLegalResponsible = $legalResponsibleRepository->findOneBy(['email'=> $legalResponsible->getEmail()])){
////                $oldLegalResponsible->addRegistration($registration);
//                $registration->addLegalResponsible($oldLegalResponsible);
//                $entityManager->persist($registration);
//                $entityManager->flush();
//
//                return $this->redirectToRoute('student_view_all_active');
//            }
//            $hashedPassword = $passwordHasher->hashPassword($legalResponsible, $legalResponsible->getPassword());
//            $legalResponsible->setPassword($hashedPassword);
//
////            $legalResponsible->addRegistration($registration);
//            $registration->addLegalResponsible($legalResponsible);
//
//            $entityManager->persist($legalResponsible);
//            $entityManager->flush();
//
//            return $this->redirectToRoute('student_view_all_active');
//        }
//
//        return $this->render('Forms/formsView.html.twig', ['formLegalResponsible' => $form->createView()]);
//    }
//

}
