<?php

namespace App\Controller\UsersController;

use App\Entity\SchoolClasses\SchoolClass;
use App\Entity\Subjects\CommonSubject;
use App\Entity\Subjects\OptionalSubject;
use App\Entity\Users\Students\LegalResponsible;
use App\Entity\Users\Students\LegalResponsibleStudent;
use App\Entity\Users\Students\Registration;
use App\Entity\Users\Students\Student;
use App\Entity\Users\User;
use App\Form\LegalResponsible\LegalResponsibleType;
use App\Form\Registration\RegistrationEditType;
use App\Form\Registration\RegistrationType;
use App\Form\RegistrationSchoolLevelType;
use App\Form\Student\LegalResponsibleStudentType;
use App\Form\Student\StudentEditType;
use App\Service\GenerateLoginAndPassword;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/eleves', name: 'student')]
//#[IsGranted('IS_AUTHENTICATED_FULLY')
class StudentController extends AbstractController
{
    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    #[IsGranted("ROLE_SCOLARITE")]
    public function viewAllStudentAction(int $currentPage, EntityManagerInterface $entityManager): Response
    {
        $studentRepository = $entityManager->getRepository(Student::class);

        if(!($users = $studentRepository->findAllWithPages($currentPage, $this->getParameter('maxResult'))))
        {
            throw $this->createNotFoundException("pas d'élève à afficher");
        }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw $this->createNotFoundException("numéro de page incorrect");
        }

        return $this->render('listView/ListUsers.html.twig', ['users' => $users,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

    #[Route('/inscrire-eleve', name: '_scolarite_register')]
    #[IsGranted("ROLE_SCOLARITE")]
    public function selectSchoolLevelAction(Request $request) : Response
    {
        $form = $this->createForm(RegistrationSchoolLevelType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            return $this->redirectToRoute('student_schoolLevel_scolarite_register', ['schoolLevelId'=> $form->get('schoolLevel')->getData()->getId()]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'studentSchoolLevel'=>'']);

    }


    #[Route('/inscrire-eleve-{schoolLevelId}', name: '_schoolLevel_scolarite_register')]
    #[IsGranted("ROLE_SCOLARITE")]
    public function addStudentAction(int $schoolLevelId, GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request): Response
    {
        $student = (new Student());
        $lrs1 = (new LegalResponsibleStudent())
            ->setStudent($student);

        $lrs2 = (new LegalResponsibleStudent())
            ->setStudent($student);

        $registration = (new Registration())
            ->setStudent($student);

        $form = $this->createForm(RegistrationType::class, $registration);
        $form->add('schoolClass', EntityType::class, [
                'class' => SchoolClass::class,
                'choice_label' => 'name',
                'label' => 'Classe',
                'query_builder' => function (EntityRepository $er) use ($schoolLevelId) {
                    return $er->createQueryBuilder('sc')
                        ->where("sc.schoolLevel = $schoolLevelId");
                },
            ]);
        $form->add('optionalSubjects', EntityType::class, [
                'class' => OptionalSubject::class,
                'choice_label' => 'name',
                'label' => 'Matière facultatives',
                'expanded' => true,
                'multiple'=> true,
                'by_reference' => false,
                'query_builder' => function (EntityRepository $er) use ($schoolLevelId) {
                    return $er->createQueryBuilder('os')
                        ->leftJoin('os.areTaught', 'it')
                        ->where("it.subject = os")
                        ->andWhere("it.schoolLevel = $schoolLevelId");
                },
            ]);

        $form->add('legalResponsibleStudent1', LegalResponsibleStudentType::class,[
                'data' => $lrs1,
                'mapped' => false,
                'required' => true
            ]);

        $form->add('legalResponsibleStudent2', LegalResponsibleStudentType::class,[
                'data' => $lrs2,
                'mapped' => false,
                'required' => false
            ]);

        $form->add('send', SubmitType::class, ['label' => 'Ajouter l\'élève']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

                foreach ($student->getLegalResponsibleStudents()->getValues() as $lr){
                    $generateLoginAndPassword->generate($lr->getLegalresponsible());
                }

                $generateLoginAndPassword->generate($student);

                $entityManager->persist($registration);
                $entityManager->persist($student);

                dump($student);
                dump($registration);

                $entityManager->flush();

                $this->addFlash('info', "L'élève a été créé");
                return $this->redirectToRoute('accueil');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'student'=>'']);
    }

    #[Route('/reinscrire-eleve-{idStudent}', name: '_scolarite_re-register')]
    #[IsGranted("ROLE_SCOLARITE")]
    public function selectSchoolLevelReRegisterAction(mixed $idStudent, Request $request) : Response
    {
        $form = $this->createForm(RegistrationSchoolLevelType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()){

            return $this->redirectToRoute('student_schoolLevel_scolarite_re-register', [
                'idStudent' => $idStudent,
                'schoolLevelId'=> $form->get('schoolLevel')->getData()->getId(),
            ]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'studentSchoolLevel'=>'']);

    }

    #[Route("/reinscrire-eleve/{idStudent}/{schoolLevelId}", name: "_schoolLevel_scolarite_re-register")]
    #[IsGranted("ROLE_SCOLARITE")]
    public function studentScolariteReRegistrationAction(int $idStudent, int $schoolLevelId, GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request) : Response
    {
        if(!($student = $entityManager->getRepository(Student::class)->find($idStudent))){
            throw $this->createNotFoundException("L'élève n'existe pas");
        }

        $registration = (new Registration())
            ->setStudent($student)
            ->setYear(new \DateTime);

        $form = $this->createForm(RegistrationType::class, $registration);
        $form->add('schoolClass', EntityType::class, [
            'class' => SchoolClass::class,
            'choice_label' => 'name',
            'label' => 'Classe',
            'query_builder' => function (EntityRepository $er) use ($schoolLevelId) {
                return $er->createQueryBuilder('sc')
                    ->where("sc.schoolLevel = $schoolLevelId");
            },
        ]);
        $form->add('optionalSubjects', EntityType::class, [
            'class' => OptionalSubject::class,
            'choice_label' => 'name',
            'label' => 'Matière facultatives',
            'expanded' => true,
            'multiple'=> true,
            'by_reference' => false,
            'query_builder' => function (EntityRepository $er) use ($schoolLevelId) {
                return $er->createQueryBuilder('os')
                    ->leftJoin('os.areTaught', 'at')
                    ->where("at.subject = os")
                    ->andWhere("at.schoolLevel = $schoolLevelId");
            },
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->persist($registration);
                $entityManager->persist($student);

                $entityManager->flush();

                $this->addFlash('info', "L'élève a été créé");
                return $this->redirectToRoute('student_view_all');

            } catch (\Exception $e) {
                $this->addFlash('error', "L'élève est déjà inscrit pour l'année en cours.");
            }
        }
        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'student'=>'']);
    }

    #[Route("/modifier-eleve-{idStudent}-{route}", name: "_scolarite_modifications")]
    #[IsGranted("ROLE_SCOLARITE")]
    public function studentScolariteModificationsAction(int $idStudent, EntityManagerInterface $entityManager, Request $request) : Response
    {
        if(!($student = $entityManager->getRepository(Student::class)->find($idStudent))){
            throw $this->createNotFoundException("L'élève n'existe pas");
        }

        $form = $this->createForm(StudentEditType::class, $student);

        $form->add('send', SubmitType::class, ['label' => 'Valider les modifications.']);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager->flush();
            $this->addFlash('info', "L'élève a été modifié");
            return $this->redirectToRoute('user_view_profile', ['login' => $student->getUserIdentifier()]);
        }
        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'student'=>'']);
    }

    #[Route('/désactiver-{id}',name: '_disable', requirements: ['id' => '\d+'])]
    #[IsGranted('ROLE_SCOLARITE')]
    public function disableUserAction(int $id, EntityManagerInterface $entityManager) : RedirectResponse
    {
        $staffRepository = $entityManager->getRepository(User::class);
        if(!($staff = $staffRepository->find($id))){
            throw new NotFoundHttpException("Ce membre du Staff n'existe pas.");
        }

        $staff->setDisabled(true);
        $entityManager->persist($staff);
        $entityManager->flush();

        $this->addFlash('info', "L'élève a été désactivé");

        return $this->redirectToRoute('student_view_all');
    }

    #[Route('/activer-{id}',name: '_enable', requirements: ['id' => '\d+'])]
    #[IsGranted('ROLE_SCOLARITE')]
    public function enableUserAction(int $id, EntityManagerInterface $entityManager) : Response
    {
        $staffRepository = $entityManager->getRepository(User::class);

        if(!($staff = $staffRepository->find($id))){
            throw new NotFoundHttpException("Ce membre du staff n'existe pas !");
        }

        $staff->setDisabled(false);
        $entityManager->persist($staff);
        $entityManager->flush();

        $this->addFlash('info', "L'élève a été activé");
        return $this->redirectToRoute('student_view_all');
    }

    #[Route("/ajouter-responsableLegal-{login}", name: "_add_legalResponsible")]
    #[IsGranted("ROLE_SCOLARITE")]
    public function addLegalResponsibleToStudent(string $login, EntityManagerInterface $entityManager, Request $request) : Response|RedirectResponse
    {
        $student = $entityManager->getRepository(Student::class)->findOneByLogin($login);

        $lrsCount = $entityManager->getRepository(LegalResponsibleStudent::class)->countLegalResponsibleFromStudent($student);

        if ($lrsCount >= 2) {
            //throw $this->createNotFoundException('Cet étudiant possède déjà 2 responsables legaux');
            $this->addFlash('error','Cet étudiant possède déjà 2 responsables legaux');
            return $this->redirectToRoute('user_view_profile', ['login' => $student->getLogin()]);
        }

        $lrs = (new LegalResponsibleStudent)
            ->setStudent($student);

        $form = $this->createForm(LegalResponsibleStudentType::class, $lrs);

        $form->add('send', SubmitType::class, ['label'=> 'Ajouter le responsable']);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $lrs->setResponsibleNumber($lrsCount+1);
            $entityManager->persist($lrs);
            $entityManager->flush();
            return $this->redirectToRoute('user_view_profile', ['login' => $student->getLogin()]);
        }

        dump($form);
        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'legalResponsible' => '']);
    }

    #[Route("/supprimer-responsableLegal-{login}", name: "_del_legalResponsible")]
    #[IsGranted("ROLE_SCOLARITE")]
    public function delLegalResponsibleFromStudent(string $login, EntityManagerInterface $entityManager, Request $request) : Response|RedirectResponse
    {
        //TODO
    }
}
