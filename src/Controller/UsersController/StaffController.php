<?php

namespace App\Controller\UsersController;

use App\Entity\Users\Staff\Staff;
use App\Form\StaffType;
use App\Service\GenerateLoginAndPassword;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('accueil/personnel', name: 'staff')]
#[IsGranted('ROLE_RH')]
class StaffController extends AbstractController
{
    #[Route('/liste/{currentPage}', name: '_view_all',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function listStaffAction(int $currentPage,EntityManagerInterface $entityManager): Response
    {
        $staffRepository = $entityManager->getRepository(Staff::class);

        if(!($users = $staffRepository->findAllPage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas de personnel à afficher");
        }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }

        return $this->render('listView/ListUsers.html.twig', ['users' => $users,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

    #[Route('/documentation/liste/{currentPage}', name: '_view_all_documentation',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function viewAllDocumentationAction(int $currentPage,EntityManagerInterface $entityManager): Response
    {
        $staffRepository = $entityManager->getRepository(Staff::class);


         if(!($users = $staffRepository->findAllDocumentationPage($currentPage, $this->getParameter('maxResult'))))
         {
             throw new NotFoundHttpException("pas de compte documentation");
         }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }

        return $this->render('listView/ListUsers.html.twig', ['users' => $users,"nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

    #[Route('/documentation/ajouter', name: '_add_documentation')]
    public function addDocumentationAction(GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $staff = (new Staff())
            ->setDisabled(false)
            ->setRoles(["ROLE_DOCUMENTATION", "ROLE_STAFF"]);

        $form = $this->createForm(StaffType::class, $staff);
        $form->add('send', SubmitType::class, ['label' => 'Ajouter un Documentaliste']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $generateLoginAndPassword->generate($staff);

//            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
//            $staff->setPassword($hashedPassword);
            $entityManager->persist($staff);
            $entityManager->flush();

            $this->addFlash('info', "Le compte documentation a été créé");
            return $this->redirectToRoute('staff_view_all_documentation');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'staff'=>'']);
    }

    #[Route('/scolarite/liste/{currentPage}', name: '_view_all_scolarite',requirements: ['currentPage' => '\d+'], defaults: ['currentPage' => 1])]
    public function viewAllScolariteAction(int $currentPage,EntityManagerInterface $entityManager): Response
    {
//        $staffRepository = $entityManager->getRepository(Staff::class);
//        $scolarite = $staffRepository->findAllScolarite();
        $staffRepository = $entityManager->getRepository(Staff::class);
        //$users = $staffRepository->findAll();


        if(!($users = $staffRepository->findAllScolaritePage($currentPage, $this->getParameter('maxResult'))))
        {
            throw new NotFoundHttpException("pas de professeurs");
        }

        $nbTotalPages = intval(ceil(count($users) / $this->getParameter('maxResult')));
        if($nbTotalPages == 0){
            $nbTotalPages = 1;
        }
        if($currentPage > $nbTotalPages)
        {
            throw new NotFoundHttpException("numéro de page incorrect");
        }

        return $this->render('listView/ListUsers.html.twig', ['users' => $users, "nbTotalPages"=> $nbTotalPages, "currentPage" => $currentPage]);
    }

    #[Route('/scolarite/ajouter', name: '_add_scolarite')]
    public function addScolariteAction(GenerateLoginAndPassword $generateLoginAndPassword, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $staff = (new Staff())
            ->setDisabled(false)
            ->setRoles(["ROLE_SCOLARITE", "ROLE_STAFF"]);

        $form = $this->createForm(StaffType::class, $staff);
        $form->add('send', SubmitType::class, ['label' => 'Ajouter un compte Scolarité']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $generateLoginAndPassword->generate($staff);

//            $hashedPassword = $passwordHasher->hashPassword($staff, $staff->getPassword());
//            $staff->setPassword($hashedPassword);

            $entityManager->persist($staff);
            $entityManager->flush();

            $this->addFlash('info', "Le compte scolarité a été créé");
            return $this->redirectToRoute('staff_view_all_scolarite');
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'staff'=>'']);
    }

    #[Route('/modifier-{staffId}-{route}', name: '_edit')]
    public function editStaffAction(int $staffId, string $route , EntityManagerInterface $entityManager, Request $request): Response
    {
        $staff = $entityManager->getRepository(Staff::class)->find($staffId);

        $form = $this->createForm(StaffType::class, $staff);
        $form->add('send', SubmitType::class, ['label' => 'Modifier le compte']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($staff);
            $entityManager->flush();

            $this->addFlash('info', "Le compte a été modifié.");
            return $this->redirectToRoute($route, ['login'=>$staff->getUserIdentifier()]);
        }

        return $this->render('forms/formsView.html.twig', ['form' => $form->createView(), 'staff'=>'']);
    }



}
