<?php

namespace App\Service;

use App\Entity\Users\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GenerateLoginAndPassword implements EventSubscriber
{
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $passwordHasher;
    private HttpClientInterface $client;
    private string $urlRandomUser = "http://randomuser.me/api/?nat=fr";

    private string $env;
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher,
        HttpClientInterface $client,
        ParameterBagInterface $parameterBag)
    {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
        $this->client = $client;
        $this->env = $parameterBag->get('APP_ENV');
    }

    public function getSubscribedEvents(): array
    {
        return [Events::prePersist];
    }

    public function prePersist(LifecycleEventArgs $args) : void {
        $entity = $args->getObject();
        if ($entity instanceof User){
            $this->generate($entity);
        }
    }

    public function generate(User $user) : void
    {
        //A commenter après avoir load le fixtures
        if (in_array("ROLE_STUDENT", $user->getRoles()) || in_array("ROLE_LEGAL_RESPONSIBLE", $user->getRoles())){
            $randUsr = $this->getRandomUser();

            $user->setFirstname($this->getFirstName($randUsr));
            $user->setName($this->getLastName($randUsr));
        }

        $user->setLoginPrefix(strtolower($user->getFirstname()[0] . substr($this->enleverCaracteresSpeciaux($user->getName()), 0, 5)));

        setlocale(LC_CTYPE, 'FR_fr');
        $loginPrefix = iconv('UTF-8', 'ASCII//Original' ,$user->getLoginPrefix());


        $maxSuffix = $this->entityManager->getRepository(User::class)->count(['loginPrefix'=>$loginPrefix]);

        $user->setLoginSuffix($maxSuffix+1);

        $loginSuffix = $user->getLoginSuffix();

        $user->setLogin("$loginPrefix$loginSuffix");

        $user->setPassword($this->passwordHasher->hashPassword($user, "123"));
    }

    private function enleverCaracteresSpeciaux(string $chaine)
    {
            $utf8 = array(
                '/[áàâãªä]/u' => 'a',
                '/[ÁÀÂÃÄ]/u' => 'A',
                '/[ÍÌÎÏ]/u' => 'I',
                '/[íìîï]/u' => 'i',
                '/[éèêë]/u' => 'e',
                '/[ÉÈÊË]/u' => 'E',
                '/[óòôõºö]/u' => 'o',
                '/[ÓÒÔÕÖ]/u' => 'O',
                '/[úùûü]/u' => 'u',
                '/[ÚÙÛÜ]/u' => 'U',
                '/ç/' => 'c',
                '/Ç/' => 'C',
                '/ñ/' => 'n',
                '/Ñ/' => 'N',
                '/-/' => ''
            );
            return preg_replace(array_keys($utf8), array_values($utf8), $chaine);

    }

    private  function getRandomUser() : array
    {
        return $this->client->request('GET', $this->urlRandomUser)->toArray();
    }

    private function getFirstName(array $randUsr) : string
    {
        return $randUsr['results'][0]['name']['first'];
    }

    private function getLastName(array $randUsr) : string
    {
        return $randUsr['results'][0]['name']['last'];
    }
}