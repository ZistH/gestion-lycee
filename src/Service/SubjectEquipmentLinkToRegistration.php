<?php

namespace App\Service;

use App\Entity\Subjects\CommonSubject;
use App\Entity\Subjects\IsTaught;
use App\Entity\Subjects\Language;
use App\Entity\Subjects\OptionalSubject;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;


/**
 *
 */
class SubjectEquipmentLinkToRegistration implements EventSubscriber
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
    }
    public function getSubscribedEvents() :array
    {
        return [Events::postPersist, Events::postUpdate];
    }

    public function postPersist(LifecycleEventArgs $args) : void
    {
        $entity = $args->getObject();
        if (!($entity instanceof IsTaught)){
            return;
        }
        $this->linkLanguages($entity);

        $this->linkOptionalSubject($entity);

        $this->linkCommonSubject($entity);

        $this->entityManager->flush();
    }

    public function postUpdate(LifecycleEventArgs $args) : void
    {
        $entity = $args->getObject();
        if (!($entity instanceof IsTaught)){
            return;
        }
        $this->linkLanguages($entity);

        $this->linkOptionalSubject($entity);

        if(!($entity->getSubject() instanceof CommonSubject)){
            return;
        }

        $schoolClasses = $entity->getSchoolLevel()->getSchoolClasses();

        foreach ($schoolClasses as $class){
            foreach ($class->getRegistrations() as $registration) {
                foreach ($entity->getEquipments() as $equipment) {
                    $registration->addEquipment($equipment);
                }
            }
        }
        $this->entityManager->flush();
    }



    private function linkLanguages(IsTaught $entity) : void
    {
        if ($entity->getSubject() instanceof Language){
            $schoolClasses = $entity->getSchoolLevel()->getSchoolClasses();

            foreach ($schoolClasses as $class){
                foreach ($class->getRegistrations() as $registration) {
                    if($registration->getLanguage()===$entity->getSubject()){
                        foreach ($entity->getEquipments() as $equipment) {
                            $registration->addEquipment($equipment);
                            $this->entityManager->persist($registration);
                            $this->entityManager->flush();
                        }
                    }
                }
            }
            $this->entityManager->flush();
        }
    }

    private function linkOptionalSubject(IsTaught $entity) : void
    {
        if ($entity->getSubject() instanceof OptionalSubject){
            $schoolClasses = $entity->getSchoolLevel()->getSchoolClasses();
            foreach ($schoolClasses as $class){
                foreach ($class->getRegistrations() as $registration) {
                    foreach ($registration->getOptionalSubjects() as $subject) {
                        if($subject===$entity->getSubject()){
                            foreach ($entity->getEquipments() as $equipment) {
                                $registration->addEquipment($equipment);
                                $this->entityManager->persist($registration);
                                $this->entityManager->flush();
                            }
                        }
                    }
                }
            }
            $this->entityManager->flush();
        }
    }

    private function linkCommonSubject(IsTaught $entity) : void
    {
        if(!($entity->getSubject() instanceof CommonSubject)){
            return;
        }

        $schoolClasses = $entity->getSchoolLevel()->getSchoolClasses();


        foreach ($schoolClasses as $class){
            foreach ($class->getRegistrations() as $registration) {
                foreach ($entity->getEquipments() as $equipment) {
                    $registration->addEquipment($equipment);
                    $this->entityManager->persist($registration);
                    $this->entityManager->flush();
                }
            }
        }
    }
}