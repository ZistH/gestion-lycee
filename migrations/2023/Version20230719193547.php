<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230719193547 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, street_number VARCHAR(255) NOT NULL, flat_number VARCHAR(255) DEFAULT NULL, street_name VARCHAR(255) NOT NULL, postalcode VARCHAR(255) NOT NULL, town VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE common_subject (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE is_taught (id INT AUTO_INCREMENT NOT NULL, school_level_id INT DEFAULT NULL, subject_id INT DEFAULT NULL, INDEX IDX_3E5FE4D7A1F77FE3 (school_level_id), INDEX IDX_3E5FE4D723EDC87 (subject_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE legal_responsible (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE legal_responsible_student (id INT AUTO_INCREMENT NOT NULL, student_id INT DEFAULT NULL, legalresponsible_id INT DEFAULT NULL, responsible_number INT NOT NULL, INDEX IDX_AB571734CB944F1A (student_id), INDEX IDX_AB57173420789B05 (legalresponsible_id), UNIQUE INDEX UNIQ_AB57173420789B05CB944F1A (legalresponsible_id, student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE optional_subject (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE optional_subject_registration (optional_subject_id INT NOT NULL, registration_id INT NOT NULL, INDEX IDX_A59F55F06DCAD8A6 (optional_subject_id), INDEX IDX_A59F55F0833D8F43 (registration_id), PRIMARY KEY(optional_subject_id, registration_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE param (id INT AUTO_INCREMENT NOT NULL, tag VARCHAR(10) NOT NULL, name VARCHAR(50) NOT NULL, libelle VARCHAR(255) NOT NULL, int_value INT DEFAULT NULL, string_value VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE registration (id INT AUTO_INCREMENT NOT NULL, student_id INT DEFAULT NULL, school_class_id INT DEFAULT NULL, language_id INT NOT NULL, YEAR INT NOT NULL, INDEX IDX_62A8A7A7CB944F1A (student_id), INDEX IDX_62A8A7A714463F54 (school_class_id), INDEX IDX_62A8A7A782F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_class (id INT AUTO_INCREMENT NOT NULL, school_level_id INT DEFAULT NULL, max_registrations INT NOT NULL, name VARCHAR(255) DEFAULT NULL, INDEX IDX_33B1AF85A1F77FE3 (school_level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_class_teaching_contract (school_class_id INT NOT NULL, teaching_contract_id INT NOT NULL, INDEX IDX_6D28F10C14463F54 (school_class_id), INDEX IDX_6D28F10CB9DC9CD6 (teaching_contract_id), PRIMARY KEY(school_class_id, teaching_contract_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_level (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, nb_discipline_fac_max INT NOT NULL, UNIQUE INDEX UNIQ_44107A095E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE staff (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT NOT NULL, current_registration_id INT DEFAULT NULL, ine VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B723AF33C4C15541 (current_registration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subject (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, disabled TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teacher (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teaching_contract (id INT AUTO_INCREMENT NOT NULL, teacher_id INT DEFAULT NULL, subject_id INT DEFAULT NULL, YEAR INT NOT NULL, INDEX IDX_67E6F86341807E1D (teacher_id), INDEX IDX_67E6F86323EDC87 (subject_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, address_id INT DEFAULT NULL, login VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, telephone VARCHAR(15) DEFAULT NULL, disabled TINYINT(1) NOT NULL, login_prefix VARCHAR(255) NOT NULL, login_suffix INT NOT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1D1C63B3AA08CB10 (login), INDEX IDX_1D1C63B3F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE common_subject ADD CONSTRAINT FK_7BD06F22BF396750 FOREIGN KEY (id) REFERENCES subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE is_taught ADD CONSTRAINT FK_3E5FE4D7A1F77FE3 FOREIGN KEY (school_level_id) REFERENCES school_level (id)');
        $this->addSql('ALTER TABLE is_taught ADD CONSTRAINT FK_3E5FE4D723EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE language ADD CONSTRAINT FK_D4DB71B5BF396750 FOREIGN KEY (id) REFERENCES subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE legal_responsible ADD CONSTRAINT FK_82208809BF396750 FOREIGN KEY (id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE legal_responsible_student ADD CONSTRAINT FK_AB571734CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE legal_responsible_student ADD CONSTRAINT FK_AB57173420789B05 FOREIGN KEY (legalresponsible_id) REFERENCES legal_responsible (id)');
        $this->addSql('ALTER TABLE optional_subject ADD CONSTRAINT FK_115BC47BBF396750 FOREIGN KEY (id) REFERENCES subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE optional_subject_registration ADD CONSTRAINT FK_A59F55F06DCAD8A6 FOREIGN KEY (optional_subject_id) REFERENCES optional_subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE optional_subject_registration ADD CONSTRAINT FK_A59F55F0833D8F43 FOREIGN KEY (registration_id) REFERENCES registration (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A7CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A714463F54 FOREIGN KEY (school_class_id) REFERENCES school_class (id)');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A782F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE school_class ADD CONSTRAINT FK_33B1AF85A1F77FE3 FOREIGN KEY (school_level_id) REFERENCES school_level (id)');
        $this->addSql('ALTER TABLE school_class_teaching_contract ADD CONSTRAINT FK_6D28F10C14463F54 FOREIGN KEY (school_class_id) REFERENCES school_class (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_class_teaching_contract ADD CONSTRAINT FK_6D28F10CB9DC9CD6 FOREIGN KEY (teaching_contract_id) REFERENCES teaching_contract (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE staff ADD CONSTRAINT FK_426EF392BF396750 FOREIGN KEY (id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33C4C15541 FOREIGN KEY (current_registration_id) REFERENCES registration (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33BF396750 FOREIGN KEY (id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE teacher ADD CONSTRAINT FK_B0F6A6D5BF396750 FOREIGN KEY (id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE teaching_contract ADD CONSTRAINT FK_67E6F86341807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id) ON DELETE SET NULL ');
        $this->addSql('ALTER TABLE teaching_contract ADD CONSTRAINT FK_67E6F86323EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE utilisateur ADD CONSTRAINT FK_1D1C63B3F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE common_subject DROP FOREIGN KEY FK_7BD06F22BF396750');
        $this->addSql('ALTER TABLE is_taught DROP FOREIGN KEY FK_3E5FE4D7A1F77FE3');
        $this->addSql('ALTER TABLE is_taught DROP FOREIGN KEY FK_3E5FE4D723EDC87');
        $this->addSql('ALTER TABLE language DROP FOREIGN KEY FK_D4DB71B5BF396750');
        $this->addSql('ALTER TABLE legal_responsible DROP FOREIGN KEY FK_82208809BF396750');
        $this->addSql('ALTER TABLE legal_responsible_student DROP FOREIGN KEY FK_AB571734CB944F1A');
        $this->addSql('ALTER TABLE legal_responsible_student DROP FOREIGN KEY FK_AB57173420789B05');
        $this->addSql('ALTER TABLE optional_subject DROP FOREIGN KEY FK_115BC47BBF396750');
        $this->addSql('ALTER TABLE optional_subject_registration DROP FOREIGN KEY FK_A59F55F06DCAD8A6');
        $this->addSql('ALTER TABLE optional_subject_registration DROP FOREIGN KEY FK_A59F55F0833D8F43');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A7CB944F1A');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A714463F54');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A782F1BAF4');
        $this->addSql('ALTER TABLE school_class DROP FOREIGN KEY FK_33B1AF85A1F77FE3');
        $this->addSql('ALTER TABLE school_class_teaching_contract DROP FOREIGN KEY FK_6D28F10C14463F54');
        $this->addSql('ALTER TABLE school_class_teaching_contract DROP FOREIGN KEY FK_6D28F10CB9DC9CD6');
        $this->addSql('ALTER TABLE staff DROP FOREIGN KEY FK_426EF392BF396750');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF33C4C15541');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF33BF396750');
        $this->addSql('ALTER TABLE teacher DROP FOREIGN KEY FK_B0F6A6D5BF396750');
        $this->addSql('ALTER TABLE teaching_contract DROP FOREIGN KEY FK_67E6F86341807E1D');
        $this->addSql('ALTER TABLE teaching_contract DROP FOREIGN KEY FK_67E6F86323EDC87');
        $this->addSql('ALTER TABLE utilisateur DROP FOREIGN KEY FK_1D1C63B3F5B7AF75');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE common_subject');
        $this->addSql('DROP TABLE is_taught');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE legal_responsible');
        $this->addSql('DROP TABLE legal_responsible_student');
        $this->addSql('DROP TABLE optional_subject');
        $this->addSql('DROP TABLE optional_subject_registration');
        $this->addSql('DROP TABLE param');
        $this->addSql('DROP TABLE registration');
        $this->addSql('DROP TABLE school_class');
        $this->addSql('DROP TABLE school_class_teaching_contract');
        $this->addSql('DROP TABLE school_level');
        $this->addSql('DROP TABLE staff');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE subject');
        $this->addSql('DROP TABLE teacher');
        $this->addSql('DROP TABLE teaching_contract');
        $this->addSql('DROP TABLE utilisateur');
    }
}
