--Peuplement--
/*
-----Non utillis� pour le peuplement de la base

drop procedure peuplement_address;
--drop procedure peuplement_address;

--Address--
create or replace procedure peuplement_address(nb in number) deterministic as
begin
    commit ; -- -D�finition du point de d�marrage de d�but de test
    for i in 1..nb loop
        insert into ADDRESS(STREET_NUMBER,STREET_NAME,DEPARTMENT,TOWN) VALUES
            (i,'rue du petit pied', 86000, 'Poitiers');
    end loop ;
    commit ; --Validation du peuplement
end ;
/
*/

--Equipments
create or replace procedure peuplement_equipment as
    VISBN number;
    VcountISBN NUMBER;
begin
    --Equipement Type--
    insert into equipment_type(type)VALUES('Tablettes');
    insert into equipment_type(type)VALUES('Outils');
    insert into equipment_type(type)VALUES('Manuel');
    insert into equipment_type(type)VALUES('Cahier TD');
    
    --Equipment--
        VISBN := FLOOR(DBMS_RANDOM.VALUE(
                            100000,
                            999999
                        )
                );
    Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    while VcountISBN = 1 loop
        VISBN := FLOOR(DBMS_RANDOM.VALUE(
                            100000,
                            999999
                        )
                );
          Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    end loop;
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication, isbn)VALUES(
        (select id from equipment_type where type = 'Manuel'),
        1,0,'Fran�ais','Fran�ois Scrible',2015, VISBN
    );
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication)VALUES(
        (select id from equipment_type where type = 'Cahier TD'),
        0,23.99,'Maths exercice','Mathieu Mesure',2020
    );
    
    Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    while VcountISBN = 1 loop
        VISBN := FLOOR(DBMS_RANDOM.VALUE(
                            100000,
                            999999
                        )
                );
          Select count(*) into VcountISBN from equipment where isbn = VISBN;
    end loop;
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication, isbn)VALUES(
        (select id from equipment_type where type = 'Manuel'),
        1,0,'Maths','Mathieu Mesure',2021, VISBN
    );
    Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    while VcountISBN = 1 loop
                VISBN := FLOOR(DBMS_RANDOM.VALUE(
                            100000,
                            999999
                        )
                );
          Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    end loop;
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication, isbn)VALUES(
        (select id from equipment_type where type = 'Manuel'),
        1,0,'Histoire','Ive Gemeau',2019, VISBN
    );
    Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    while VcountISBN = 1 loop
        VISBN := FLOOR(DBMS_RANDOM.VALUE(
                            100000,
                            999999
                        )
                );
          Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    end loop;
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication, isbn)VALUES(
        (select id from equipment_type where type = 'Manuel'),
        1,0,'G�ographie','Gaelle Porche',2020, VISBN
    );
    
    Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    while VcountISBN = 1 loop
        VISBN := FLOOR(DBMS_RANDOM.VALUE(
                            100000,
                            999999
                        )
                );
          Select count(*) into VcountISBN from equipment where ISBN = VISBN;
    end loop;
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication, isbn)VALUES(
        (select id from equipment_type where type = 'Manuel'),
        1,0,'Espagnol','Marco Gonzalez',2017, VISBN
    );
    
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication)VALUES(
        (select id from equipment_type where type = 'Outils'),
        1,0,'Chaussure','Jean Anma',null
    );
    insert into equipment(type_id,reusable,cost,title,author,year_of_publication)VALUES(
        (select id from equipment_type where type = 'Tablettes'),
        1,0,'Allemand','Arnold Schwarzenegger',2020
    );
end;
/

--classes
create or replace procedure peuplement_school_class(nb in number) as
    VNbSchoolLevel number;
begin
    Select count(*) into VNbSchoolLevel
        from school_level
    ;
    
    if VNbSchoolLevel<1 then
        insert into school_level(name, nb_discipline_fac_max)VALUES('Seconde',3);
        insert into school_level(name,nb_discipline_fac_max)VALUES('Premi�re',2);
        insert into school_level(name,nb_discipline_fac_max)VALUES('Terminale',1);
    end if;
    
    --School class
    for i in 1..nb loop
        insert into school_class(school_level_id,max_registrations,name)VALUES(
            (select id from school_level where name = 'Seconde'),
            40, CONCAT('Seconde ', i)
        );
        insert into school_class(school_level_id,max_registrations,name)VALUES(
            (select id from school_level where name = 'Premi�re'),
            40, CONCAT('Premi�re ', i)
        );
        insert into school_class(school_level_id,max_registrations,name)VALUES(
            (select id from school_level where name = 'Terminale'),
            40, CONCAT('Terminale ', i)
        );
    end loop;
end;
/

--Utilisateurs
--scolarite
create or replace procedure peuplement_scolarite(nb in number) as
begin
    commit ; -- -D�finition du point de d�marrage de d�but de test
    for i in 1..nb loop
        insert into ADDRESS(STREET_NUMBER,STREET_NAME,POSTALCODE,TOWN) VALUES
            (i,
                'rue de la flaque', 
                86000, 
                'Poitiers'
            );
        insert into utilisateur(address_id, login, roles, password, name, firstname, email,disabled,login_prefix,login_suffix,type) values
            ((Select id from address where street_name = 'rue de la flaque' and street_number = i),
                CONCAT('scol',i),
                '["ROLE_SCOLARITE","ROLE_STAFF"]',
                '123',
                'Col�re',
                'Sandrine',
                CONCAT(CONCAT('scol',i),'@mail.com'),
                0,
                'scol',
                i,
                'staff'
            );
    end loop ;
    commit ; --Validation du peuplement
end ;
/

--documentation
create or replace procedure peuplement_documentation(nb in number) as
begin
    commit ; -- -D�finition du point de d�marrage de d�but de test
    for i in 1..nb loop
        insert into ADDRESS(STREET_NUMBER,STREET_NAME,POSTALCODE,TOWN) VALUES
            (i,
                'rue de la biblioth�que', 
                86000, 
                'Poitiers'
            );
        insert into utilisateur(address_id, login, roles, password, name, firstname, email,disabled,login_prefix,login_suffix,type) values
            ((Select id from address where street_name = 'rue de la biblioth�que' and street_number = i),
                CONCAT('docu',i),
                '["ROLE_DOCUMENTATION","ROLE_STAFF"]',
                '123',
                'Curare',
                'Doriant',
                CONCAT(CONCAT('docu',i),'@mail.com'),
                0,
                'docu',
                i,
                'staff'
            );
    end loop ;
    commit ; --Validation du peuplement
end ;
/

--teacher
create or replace procedure peuplement_teacher(nb in number) as
    VNom varchar2(50);
    VPrenom varchar2(50);
    Vteacherid number;
    VSubjectId number;
begin
    commit ; -- -D�finition du point de d�marrage de d�but de test
    select id into VSubjectId
        from (select id from Subject order by (select count(*) from SUBJECT)) MaTable
        where rownum=1
    ;
    for i in 1..nb loop
        select NOM into VNom
            from (select STATS_NOM.NOM from STATS_NOM order by dbms_random.value) MaTable
            where rownum=1
        ;
        select Stats_Prenom into VPrenom
            from (select Stats_Prenom from Stats_Prenom order by dbms_random.value) MaTable
            where rownum=1
        ;
        insert into ADDRESS(STREET_NUMBER,STREET_NAME,POSTALCODE,TOWN) VALUES(
            i, 'rue des cours', 
            86000, 'Poitiers'
        );
        insert into utilisateur(address_id, login, roles, password, name, firstname, email, disabled, login_prefix, login_suffix, type) values
            ((Select id from address where street_name = 'rue des cours' and street_number = i),
                CONCAT('prof',i),
                '["ROLE_TEACHER","ROLE_STAFF"]',
                '123',
                VNom,
                VPrenom,
                CONCAT(CONCAT('prof',i),'@mail.com'),
                0,
                'prof',
                i,
                'teacher'
            );
            
        Select id into Vteacherid 
            from utilisateur 
            where login = CONCAT('prof',i)
        ;
        insert into teaching_contract(teacher_id, subject_id, YEAR)values
        (Vteacherid,
            VSubjectId,
            EXTRACT(YEAR FROM SYSDATE)
        );
        VSubjectId := VSubjectId+1;
        for row in (Select * from school_class) loop
            insert into school_class_teaching_contract values 
            (
                row.id,
                (select id from Teaching_contract where teaching_contract.year = EXTRACT(YEAR FROM SYSDATE) and teaching_contract.teacher_id = Vteacherid)        
            );
        end loop;
    end loop ;
    commit ; --Validation du peuplement
end ;
/

--student
create or replace procedure peuplement_student(nb in number) as
    VNom varchar2(50);
    VPrenom varchar2(50);
    Vstudentid number;
    VIdRegistration number;
    Vlegalresponsibleid number;
    Vlegalresponsible2id number;
    VINE number;
    VcountINE number;
    VidLangue number;
    VidOptionalSubject number;
    VNbMaxOptionalSubject number;
    VNbOptionalSubject number;
    VidSC number;
begin
    commit ; -- -D�finition du point de d�marrage de d�but de test
    for i in 5..nb loop
        insert into ADDRESS(STREET_NUMBER,STREET_NAME,POSTALCODE,TOWN) VALUES
            (i,
                'rue des billes', 
                86000, 
                'Poitiers'
            );
        select Nom into VNom
            from (select Nom from Stats_Nom order by dbms_random.value) MaTable
            where rownum=1
        ;
       
        select Stats_Prenom into VPrenom
            from (select Stats_Prenom from Stats_Prenom order by dbms_random.value) MaTable
            where rownum=1
        ;
        insert into utilisateur(address_id, login, roles, password, name, firstname, email,disabled,login_prefix,login_suffix,type) values
            ((Select id from address where street_name = 'rue des billes' and street_number = i),
                CONCAT('lega',i),
                '["ROLE_LEGAL_RESPONSIBLE"]',
                '123',
                VNom,
                VPrenom,
                CONCAT(CONCAT(CONCAT(Vprenom,Vnom),i),'@mail.com'),
                0,
                'lega',
                i,
                'legalResponsible'
            );
        Select id into Vlegalresponsibleid 
            from utilisateur 
            where login = CONCAT('lega',i)
        ;
        if MOD(i,5) = 0 then
            select Stats_Prenom into VPrenom
                from (select Stats_Prenom from Stats_Prenom order by dbms_random.value) MaTable
                where rownum=1
            ;
            insert into utilisateur(address_id, login, roles, password, name, firstname, email,disabled,login_prefix,login_suffix,type) values
            ((Select id from address where street_name = 'rue des billes' and street_number = i),
                CONCAT('legal',i),
                '["ROLE_LEGAL_RESPONSIBLE"]',
                '123',
                VNom,
                VPrenom,
                CONCAT(CONCAT(CONCAT(Vprenom,Vnom),i),'@mail.com'),
                0,
                'legal',
                i,
                'legalResponsible'
            );
            Select id into Vlegalresponsible2id 
                from utilisateur 
                where login = CONCAT('legal',i)
            ;
            
        end if;
         select Stats_Prenom into VPrenom
                from (select Stats_Prenom from Stats_Prenom order by dbms_random.value) MaTable
                where rownum=1
            ;
        insert into utilisateur(address_id, login, roles, password, name, firstname, email,disabled,login_prefix,login_suffix,type) values
                ((Select id from address where street_name = 'rue des billes' and street_number = i),
                    CONCAT('stud',i),
                    '["ROLE_STUDENT"]',
                    '123',
                    VNom,
                    VPrenom,
                    CONCAT(CONCAT(CONCAT(Vprenom,Vnom),i),'@mail.com'),
                    0,
                    'stud',
                    i,
                    'student'
                );
            Select id into Vstudentid 
                from utilisateur 
                where login = CONCAT('stud',i)
            ;
            
            VINE := FLOOR(DBMS_RANDOM.VALUE(
              10000 ,
              99999));
            Select count(*) into VcountINE from student where ine = VINE;
            while VcountINE = 1 loop
                VINE := FLOOR(DBMS_RANDOM.VALUE(
                  10000,
                  99999));
                  Select count(*) into VcountINE from student where ine = VINE;
            end loop;
            
            insert into student values(Vstudentid,Vlegalresponsibleid,Vlegalresponsible2id, VINE);
            
            select id into VidLangue
                from (select id from language order by dbms_random.value) MaTable
                where rownum=1
            ;

            select id into VidSC
                from (select id from school_class order by dbms_random.value) MaTable
                where rownum=1
            ;
            insert into registration(student_id, school_class_id,language_id, YEAR) values(vstudentid,
                vidSC, vidlangue, EXTRACT(YEAR FROM SYSDATE));
                
            select id into VIdRegistration
                from registration
                where student_id = vstudentid
                and  year = EXTRACT(YEAR FROM SYSDATE)
            ;
            
            select SL.NB_DISCIPLINE_FAC_MAX into VNbMaxOptionalSubject
                from School_Level SL, School_Class SC
                where sc.id = VidSc
                and sc.school_level_id = sl.id
            ;
            
            select count(*) into VNbOptionalSubject
                from Optional_subject
            ;
            if VNbOptionalSubject < VNbMaxOptionalSubject then
                VNbMaxOptionalSubject := VNbOptionalSubject;
            end if;
            
            for rec in 1..FLOOR(DBMS_RANDOM.VALUE(0,VNbMaxOptionalSubject)) loop
                select id into VidOptionalSubject
                    from (select id from Optional_subject order by dbms_random.value) MaTable
                    where rownum=1
                ;
                insert into Optional_subject_registration values (
                    VidOptionalSubject, VIdRegistration
                );
            end loop;
    end loop ;
    commit ; --Validation du peuplement
end ;
/

--is_teaches
create or replace procedure peuplement_isteaches as
    VnomMatiere Varchar2(50);
    VMatiereId number;
    VIsTeachesId number;
    VequipmentId number;
begin
    for r in (Select id, name from subject) loop
        for row in (Select id from school_level) loop        
            insert into IS_TAUGHT(school_level_id, subject_id) values(row.id, r.id);
            VnomMatiere := r.name;
            select id into VIsTeachesId 
                from IS_TAUGHT
                where school_level_id = row.id 
                and subject_id = r.id
            ;
            if(VnomMatiere = 'Fran�ais') then
                select id into VequipmentId from equipment where title = 'Fran�ais';
                insert into IS_TAUGHT_equipment values (VIsTeachesId, VequipmentId);
            end if;
            if(VnomMatiere = 'Maths') then
                insert into IS_TAUGHT_equipment values
                (VIsTeachesId, 
                    (
                        select id from equipment where title = 'Maths'
                    )
                );
                insert into IS_TAUGHT_equipment values
                (VIsTeachesId, 
                    (
                        select id from equipment where title = 'Maths exercice'
                    )
                );
            end if;
            if(VnomMatiere = 'Histoire-Geo') then
                insert into IS_TAUGHT_equipment values
                (VIsTeachesId, 
                    (
                        select id from equipment where title = 'Histoire'
                    )
                );
                insert into IS_TAUGHT_equipment values
                (VIsTeachesId, 
                    (
                        select id from equipment where title = 'G�ographie'
                    )
                );
            end if;
        end loop;
    end loop;    
end;
/
commit;
