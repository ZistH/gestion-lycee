create sequence EQUIPMENT_ID_SEQ
    /

create sequence EQUIPMENT_TYPE_ID_SEQ
    /

create sequence HIGH_SCHOOL_TYPE_ID_SEQ
    /

create sequence IS_NOTED_ID_SEQ
    /

create sequence IS_TEACHES_ID_SEQ
    /

create sequence REGISTRATION_ID_SEQ
    /

create sequence SCHOOL_CLASS_ID_SEQ
    /

create sequence SCHOOL_LEVEL_ID_SEQ
    /

create sequence SECTION_ID_SEQ
    /

create sequence STATEMENT_ID_SEQ
    /

create sequence SUBJECT_ID_SEQ
    /

create sequence TEACHING_CONTRACT_ID_SEQ
    /

create sequence UTILISATEUR_ID_SEQ
    /

create sequence ADDRESS_ID_SEQ
    /

create table DOCTRINE_MIGRATION_VERSIONS
(
    VERSION        VARCHAR2(191) not null
        primary key,
    EXECUTED_AT    TIMESTAMP(0) default NULL,
    EXECUTION_TIME NUMBER(10)   default NULL
)
    /

create table ADDRESS
(
    ID            NUMBER(10)    not null
        primary key,
    STREET_NUMBER NUMBER(10)    not null,
    FLAT_NUMBER   NUMBER(10) default NULL,
    STREET_NAME   VARCHAR2(255) not null,
    DEPARTMENT    NUMBER(10)    not null,
    TOWN          VARCHAR2(255) not null
)
    /

create trigger AUTOADDRESS
    before insert
    on ADDRESS
    for each row
begin
    select "L3PRO24"."ADDRESS_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table EQUIPMENT_TYPE
(
    ID   NUMBER(10)    not null
        primary key,
    TYPE VARCHAR2(255) not null
)
    /

create table EQUIPMENT
(
    ID                  NUMBER(10) not null
        primary key,
    TYPE_ID             NUMBER(10)    default NULL
        constraint FK_D338D583C54C8C93
        references EQUIPMENT_TYPE,
    REUSABLE            NUMBER(1)  not null,
    COST                FLOAT      not null,
    TITLE               VARCHAR2(255) default NULL,
    AUTHOR              VARCHAR2(255) default NULL,
    YEAR_OF_PUBLICATION NUMBER(10)    default NULL
)
    /

create index IDX_D338D583C54C8C93
    on EQUIPMENT (TYPE_ID)
    /

create trigger AUTOEQUIPMENT
    before insert
    on EQUIPMENT
    for each row
begin
    select "L3PRO24"."EQUIPMENT_ID_SEQ".nextval into :new.Id from dual;
end;
/

create trigger EQUIPMENT_YEAR_OF_PUBLICATION
    after insert or update
                        on EQUIPMENT
                        for each row
begin
        if :new.YEAR_OF_PUBLICATION<2000 or :new.YEAR_OF_PUBLICATION>TO_CHAR(SYSDATE, 'YYYY') then
            raise_application_error(-21000, 'année entrée non valide');
end if;
end;
/

create unique index UNIQ_B65A862F8CDE5729
    on EQUIPMENT_TYPE (TYPE)
    /

create trigger AUTOEQUIPMENTTYPE
    before insert
    on EQUIPMENT_TYPE
    for each row
begin
    select "L3PRO24"."EQUIPMENT_TYPE_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table HIGH_SCHOOL_TYPE
(
    ID   NUMBER(10)    not null
        primary key,
    NAME VARCHAR2(255) not null
)
    /

create trigger AUTOHIGHSCHOOLTYPE
    before insert
    on HIGH_SCHOOL_TYPE
    for each row
begin
    select "L3PRO24"."HIGH_SCHOOL_TYPE_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table SCHOOL_LEVEL
(
    ID                    NUMBER(10)    not null
        primary key,
    NAME                  VARCHAR2(255) not null,
    NB_DISCIPLINE_FAC_MAX NUMBER(10)    not null
        constraint NB_DISCIPLINE_FAC_MAX
        check (NB_Discipline_fac_max >= 0 and NB_Discipline_fac_max <= 3)
)
    /

create table SCHOOL_CLASS
(
    ID                NUMBER(10) not null
        primary key,
    SCHOOL_LEVEL_ID   NUMBER(10)    default NULL
        constraint FK_33B1AF85A1F77FE3
        references SCHOOL_LEVEL,
    MAX_REGISTRATIONS NUMBER(10) not null
        constraint MAX_REGISTRATIONS
        check (MAX_REGISTRATIONS >= 0 and MAX_REGISTRATIONS <= 40),
    NAME              VARCHAR2(255) default NULL
)
    /

create index IDX_33B1AF85A1F77FE3
    on SCHOOL_CLASS (SCHOOL_LEVEL_ID)
    /

create trigger AUTOSCHOOLCLASS
    before insert
    on SCHOOL_CLASS
    for each row
begin
    select "L3PRO24"."SCHOOL_CLASS_ID_SEQ".nextval into :new.Id from dual;
end;
/

create unique index UNIQ_44107A095E237E06
    on SCHOOL_LEVEL (NAME)
    /

create trigger AUTOSCHOOLLEVEL
    before insert
    on SCHOOL_LEVEL
    for each row
begin
    select "L3PRO24"."SCHOOL_LEVEL_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table SECTION
(
    ID                 NUMBER(10)    not null
        primary key,
    HIGH_SCOOL_TYPE_ID NUMBER(10) default NULL
        constraint FK_2D737AEF6801AAB9
        references HIGH_SCHOOL_TYPE,
    NAME               VARCHAR2(255) not null
)
    /

create index IDX_2D737AEF6801AAB9
    on SECTION (HIGH_SCOOL_TYPE_ID)
    /

create trigger AUTOSECTION
    before insert
    on SECTION
    for each row
begin
    select "L3PRO24"."SECTION_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table STATEMENT
(
    ID    NUMBER(10) not null
        primary key,
    STATE CLOB       not null
)
    /

create trigger AUTOSTATEMENT
    before insert
    on STATEMENT
    for each row
begin
    select "L3PRO24"."STATEMENT_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table SUBJECT
(
    ID         NUMBER(10)    not null
        primary key,
    NAME       VARCHAR2(255) not null,
    FACULTATIF NUMBER(1)     not null,
    DISABLED   NUMBER(1)     not null,
    TYPE       VARCHAR2(255) not null
)
    /

create table IS_NOTED
(
    ID         NUMBER(10)    not null
        primary key,
    SUBJECT_ID NUMBER(10) default NULL
        constraint FK_CE34C03B23EDC87
        references SUBJECT,
    VALUE      FLOAT         not null,
    LABEL      VARCHAR2(255) not null
)
    /

create index IDX_CE34C03B23EDC87
    on IS_NOTED (SUBJECT_ID)
    /

create trigger AUTOISNOTED
    before insert
    on IS_NOTED
    for each row
begin
    select "L3PRO24"."IS_NOTED_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table IS_TEACHES
(
    ID              NUMBER(10) not null
        primary key,
    SCHOOL_LEVEL_ID NUMBER(10) default NULL
        constraint FK_D1CD108BA1F77FE3
        references SCHOOL_LEVEL,
    SUBJECT_ID      NUMBER(10) default NULL
        constraint FK_D1CD108B23EDC87
        references SUBJECT
)
    /

create index IDX_D1CD108BA1F77FE3
    on IS_TEACHES (SCHOOL_LEVEL_ID)
    /

create index IDX_D1CD108B23EDC87
    on IS_TEACHES (SUBJECT_ID)
    /

create trigger AUTOISTEACHES
    before insert
    on IS_TEACHES
    for each row
begin
    select "L3PRO24"."IS_TEACHES_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table IS_TEACHES_EQUIPMENT
(
    IS_TEACHES_ID NUMBER(10) not null
        constraint FK_C0969E0E24369DDC
        references IS_TEACHES
        on delete cascade,
    EQUIPMENT_ID  NUMBER(10) not null
        constraint FK_C0969E0E517FE9FE
        references EQUIPMENT
        on delete cascade,
    primary key (IS_TEACHES_ID, EQUIPMENT_ID)
)
    /

create index IDX_C0969E0E24369DDC
    on IS_TEACHES_EQUIPMENT (IS_TEACHES_ID)
    /

create index IDX_C0969E0E517FE9FE
    on IS_TEACHES_EQUIPMENT (EQUIPMENT_ID)
    /

create trigger EQUIPMENT_FOR_REGISTRATION
    after insert or update
                        on IS_TEACHES_EQUIPMENT
                        for each row
declare
level_id NUMBER;
    nb_student NUMBER;
    registration_id NUmber;

begin
select SCHOOL_LEVEL_ID into level_id
from is_teaches
where is_teaches.id = :new.is_teaches_id;

for rec in (select Registration.id, Registration.student_id, Registration.school_class_id, Registration.language_id, Registration.year
                        from Registration, School_class
                        where Registration.year = TO_CHAR(SYSDATE, 'YYYY')
                        and School_class.id = registration.school_class_id
                        and school_class.school_level_id = 1
                    ) loop

            insert into registration_equipment values (
                rec.id,
                :new.equipment_id
            );

end loop;
end;
/

create table LANGUAGE
(
    ID NUMBER(10) not null
        primary key
        constraint FK_D4DB71B5BF396750
        references SUBJECT
        on delete cascade
)
    /

create table LANGUAGE_EQUIPMENT
(
    LANGUAGE_ID  NUMBER(10) not null
        constraint FK_1558AE6982F1BAF4
        references LANGUAGE
        on delete cascade,
    EQUIPMENT_ID NUMBER(10) not null
        constraint FK_1558AE69517FE9FE
        references EQUIPMENT
        on delete cascade,
    primary key (LANGUAGE_ID, EQUIPMENT_ID)
)
    /

create index IDX_1558AE6982F1BAF4
    on LANGUAGE_EQUIPMENT (LANGUAGE_ID)
    /

create index IDX_1558AE69517FE9FE
    on LANGUAGE_EQUIPMENT (EQUIPMENT_ID)
    /

create trigger AUTOSUBJECT
    before insert
    on SUBJECT
    for each row
begin
    select "L3PRO24"."SUBJECT_ID_SEQ".nextval into :new.Id from dual;
end;
/

create trigger AJOUT_MATIERE
    after insert
    on SUBJECT
    for each row
begin
    if (:new.TYPE = 'langue') then
        insert into LANGUAGE(ID) VALUES (:new.ID);
end if;
end;
/

create table UTILISATEUR
(
    ID           NUMBER(10)    not null
        primary key,
    ADDRESS_ID   NUMBER(10)    default NULL
        constraint FK_1D1C63B3F5B7AF75
        references ADDRESS,
    LOGIN        VARCHAR2(180) not null,
    ROLES        CLOB          not null,
    PASSWORD     VARCHAR2(255) not null,
    NAME         VARCHAR2(255) not null,
    FIRSTNAME    VARCHAR2(255) not null,
    EMAIL        VARCHAR2(255) not null,
    TELEPHONE    VARCHAR2(255) default NULL,
    DISABLED     NUMBER(1)     not null,
    LOGIN_PREFIX VARCHAR2(255) not null,
    LOGIN_SUFFIX NUMBER(10)    not null,
    TYPE         VARCHAR2(255) not null
)
    /

comment on column UTILISATEUR.ROLES is '(DC2Type:json)'
/

create table LEGAL_RESPONSIBLE
(
    ID NUMBER(10) not null
        primary key
        constraint FK_82208809BF396750
        references UTILISATEUR
        on delete cascade
)
    /

create table STAFF
(
    ID NUMBER(10) not null
        primary key
        constraint FK_426EF392BF396750
        references UTILISATEUR
        on delete cascade
)
    /

create table STUDENT
(
    ID                    NUMBER(10)    not null
        primary key
        constraint FK_B723AF33BF396750
        references UTILISATEUR
        on delete cascade,
    LEGAL_RESPONSIBLE1_ID NUMBER(10)    not null
        constraint FK_B723AF33F4CF9FE6
        references LEGAL_RESPONSIBLE,
    LEGAL_RESPONSIBLE2_ID NUMBER(10) default NULL
        constraint FK_B723AF33E67A3008
        references LEGAL_RESPONSIBLE,
    INE                   VARCHAR2(255) not null
)
    /

create table REGISTRATION
(
    ID              NUMBER(10) not null
        primary key,
    STUDENT_ID      NUMBER(10) default NULL
        constraint FK_62A8A7A7CB944F1A
        references STUDENT,
    SCHOOL_CLASS_ID NUMBER(10) default NULL
        constraint FK_62A8A7A714463F54
        references SCHOOL_CLASS,
    LANGUAGE_ID     NUMBER(10) not null
        constraint FK_62A8A7A782F1BAF4
        references LANGUAGE,
    YEAR            NUMBER(10) not null
)
    /

create index IDX_62A8A7A7CB944F1A
    on REGISTRATION (STUDENT_ID)
    /

create index IDX_62A8A7A714463F54
    on REGISTRATION (SCHOOL_CLASS_ID)
    /

create index IDX_62A8A7A782F1BAF4
    on REGISTRATION (LANGUAGE_ID)
    /

create trigger AUTOREGISTRATION
    before insert
    on REGISTRATION
    for each row
begin
    select "L3PRO24"."REGISTRATION_ID_SEQ".nextval into :new.Id from dual;
end;
/

create table REGISTRATION_EQUIPMENT
(
    REGISTRATION_ID NUMBER(10) not null
        constraint FK_30642BE6833D8F43
        references REGISTRATION
        on delete cascade,
    EQUIPMENT_ID    NUMBER(10) not null
        constraint FK_30642BE6517FE9FE
        references EQUIPMENT
        on delete cascade,
    primary key (REGISTRATION_ID, EQUIPMENT_ID)
)
    /

create index IDX_30642BE6833D8F43
    on REGISTRATION_EQUIPMENT (REGISTRATION_ID)
    /

create index IDX_30642BE6517FE9FE
    on REGISTRATION_EQUIPMENT (EQUIPMENT_ID)
    /

create index IDX_B723AF33F4CF9FE6
    on STUDENT (LEGAL_RESPONSIBLE1_ID)
    /

create index IDX_B723AF33E67A3008
    on STUDENT (LEGAL_RESPONSIBLE2_ID)
    /

create table TEACHER
(
    ID NUMBER(10) not null
        primary key
        constraint FK_B0F6A6D5BF396750
        references UTILISATEUR
        on delete cascade
)
    /

create table TEACHING_CONTRACT
(
    ID         NUMBER(10) not null
        primary key,
    TEACHER_ID NUMBER(10) default NULL
        constraint FK_67E6F86341807E1D
        references TEACHER,
    SUBJECT_ID NUMBER(10) default NULL
        constraint FK_67E6F86323EDC87
        references SUBJECT,
    YEAR       NUMBER(10) not null
)
    /

create table SCHOOL_CLASS_TEACHING_CONTRACT
(
    SCHOOL_CLASS_ID      NUMBER(10) not null
        constraint FK_6D28F10C14463F54
        references SCHOOL_CLASS
        on delete cascade,
    TEACHING_CONTRACT_ID NUMBER(10) not null
        constraint FK_6D28F10CB9DC9CD6
        references TEACHING_CONTRACT
        on delete cascade,
    primary key (SCHOOL_CLASS_ID, TEACHING_CONTRACT_ID)
)
    /

create index IDX_6D28F10C14463F54
    on SCHOOL_CLASS_TEACHING_CONTRACT (SCHOOL_CLASS_ID)
    /

create index IDX_6D28F10CB9DC9CD6
    on SCHOOL_CLASS_TEACHING_CONTRACT (TEACHING_CONTRACT_ID)
    /

create index IDX_67E6F86341807E1D
    on TEACHING_CONTRACT (TEACHER_ID)
    /

create index IDX_67E6F86323EDC87
    on TEACHING_CONTRACT (SUBJECT_ID)
    /

create trigger AUTOTEACHINGCONTRACT
    before insert
    on TEACHING_CONTRACT
    for each row
begin
    select "L3PRO24"."TEACHING_CONTRACT_ID_SEQ".nextval into :new.Id from dual;
end;
/

create unique index UNIQ_1D1C63B3AA08CB10
    on UTILISATEUR (LOGIN)
    /

create index IDX_1D1C63B3F5B7AF75
    on UTILISATEUR (ADDRESS_ID)
    /

create trigger AUTOUTILISATEUR
    before insert
    on UTILISATEUR
    for each row
begin
    select "L3PRO24"."UTILISATEUR_ID_SEQ".nextval into :new.Id from dual;
end;
/

create trigger AJOUT_UTILISATEUR
    after insert
    on UTILISATEUR
    for each row
begin
    if (:new.TYPE = 'staff') then
        insert into STAFF VALUES (:new.ID);
end if;
if (:new.TYPE = 'student') then
        insert into STUDENT VALUES (:new.ID, :new.legal_responsible1_id, :new.legal_responsible2_id, :new.ine);
end if;
    if (:new.TYPE = 'teacher') then
        insert into TEACHER VALUES (:new.ID);
end if;
    if (:new.TYPE = 'legalResponsible') then
        insert into LEGAL_RESPONSIBLE VALUES (:new.ID);
end if;
end;
/

create table STATS_NOM
(
    STATS_NOM VARCHAR2(50) not null
)
    /

create table STATS_PRENOM
(
    STATS_PRENOM          VARCHAR2(50) not null,
    STATS_PRENOM_MASCULIN NUMBER       not null
)
    /

create procedure peuplement_address(nb in number) deterministic as
begin
commit ; -- -Définition du point de démarrage de début de test
for i in 1..nb loop
        insert into ADDRESS(STREET_NUMBER,STREET_NAME,DEPARTMENT,TOWN) VALUES
            (i,'rue du petit pied', 86, 'Poitiers');
end loop ;
commit ; --Validation du peuplement
end ;
/

