--S�quences--

--Address
create or replace trigger AutoAddress before insert on address
for each row
begin
    select "L3PRO24"."ADDRESS_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Equipment
create or replace trigger AutoEquipment before insert on equipment
for each row
begin
    select "L3PRO24"."EQUIPMENT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Equipment type
create or replace trigger AutoEquipmentType before insert on equipment_type
for each row
begin
    select "L3PRO24"."EQUIPMENT_TYPE_ID_SEQ".nextval into :new.Id from dual;
end;
/

--High scholl type
create or replace trigger AutoHighSchoolType before insert on high_school_type
for each row
begin
    select "L3PRO24"."HIGH_SCHOOL_TYPE_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Is noted
create or replace trigger AutoIsNoted before insert on is_noted
for each row
begin
    select "L3PRO24"."IS_NOTED_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Is teaches
create or replace trigger AutoIsTeaches before insert on is_teaches
for each row
begin
    select "L3PRO24"."IS_TEACHES_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Registration
create or replace trigger AutoRegistration before insert on registration
for each row
begin
    select "L3PRO24"."REGISTRATION_ID_SEQ".nextval into :new.Id from dual;
end;
/

--School class
create or replace trigger AutoSchoolClass before insert on school_class
for each row
begin
    select "L3PRO24"."SCHOOL_CLASS_ID_SEQ".nextval into :new.Id from dual;
end;
/

--School level
create or replace trigger AutoSchoolLevel before insert on school_level
for each row
begin
    select "L3PRO24"."SCHOOL_LEVEL_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Section
create or replace trigger AutoSection before insert on section
for each row
begin
    select "L3PRO24"."SECTION_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Statement
create or replace trigger AutoStatement before insert on statement
for each row
begin
    select "L3PRO24"."STATEMENT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Subject
create or replace trigger AutoSubject before insert on subject
for each row
begin
    select "L3PRO24"."SUBJECT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Teaching contract
create or replace trigger AutoTeachingContract before insert on teaching_contract
for each row
begin
    select "L3PRO24"."TEACHING_CONTRACT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Utilisateur
create or replace trigger AutoUtilisateur before insert on utilisateur
for each row
begin
    select "L3PRO24"."UTILISATEUR_ID_SEQ".nextval into :new.Id from dual;
end;
/

/*
drop trigger AutoAddress;
drop trigger AutoEquipment;
drop trigger AutoEquipmentType;
drop trigger AutoHighSchoolType;
drop trigger AutoIsNoted;
drop trigger AutoIsTeaches;
drop trigger AutoRegistration;
drop trigger AutoSchoolClass;
drop trigger AutoSchoolLevel;
drop trigger AutoSection;
drop trigger AutoStatement;
drop trigger AutoSubject;
drop trigger AutoTeachingContract;
drop trigger AutoUtilisateur;
*/

commit;