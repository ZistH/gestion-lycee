create table test_result(
    test_name Varchar2(100),
    test_result Varchar2(100)
);

--Tests n�gatif
drop procedure test_TeacherPerSubjectPerClass;
create or replace procedure test_TeacherPerSubjectPerClass deterministic as
    VNbUserBefore number;
    VNbUserAfter number;
    VNbTeacherBefore number;
    VNbTeacherAfter number;
begin
    commit;
        
       select count(*) into VNbUserBefore from Utilisateur;
       select count(*) into VNbTeacherBefore from Teacher;
       insert into ADDRESS VALUES(
                1, 1, 'rue des cours', 
                86000, 'Poitiers'
        );
        insert into utilisateur(address_id, login, roles, password, name, firstname, email, telephone, disabled, login_prefix, login_suffix, type) values (
            1, test, login, '["ROLE_TEACHER"]', 123, test, test, test@mail.com, 0214578963, 0, test, 1, 'teacher'
        );
        insert into teacher
        select count(*) into VNbUserAfter from Utilisateur;
        select count(*) into VNbTeacherAfter from Teacher;
    rollBack;
    insert into test_result values('ajout teacher autoInc et lien Table teacher', 'r�ussit');
    exception
        when others then 
            rollBack;
            insert into test_result values('test_name', 'd�faillance');
            commit;
end;
/