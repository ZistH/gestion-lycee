--Purge des tables pour re remplissage
call purge_tables();

--Creation des trigger pour les s�quances
--Address
create or replace trigger AutoAddress before insert on address
for each row
begin
    select "MAXIME"."ADDRESS_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Equipment
create or replace trigger AutoEquipment before insert on equipment
for each row
begin
    select "MAXIME"."EQUIPMENT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Equipment type
create or replace trigger AutoEquipmentType before insert on equipment_type
for each row
begin
    select "MAXIME"."EQUIPMENT_TYPE_ID_SEQ".nextval into :new.Id from dual;
end;
/

--High scholl type
create or replace trigger AutoHighSchoolType before insert on high_school_type
for each row
begin
    select "MAXIME"."HIGH_SCHOOL_TYPE_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Is noted
create or replace trigger AutoIsNoted before insert on is_noted
for each row
begin
    select "MAXIME"."IS_NOTED_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Is teaches
create or replace trigger AutoIsTeaches before insert on IS_TAUGHT
for each row
begin
    select "MAXIME"."IS_TAUGHT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Registration
create or replace trigger AutoRegistration before insert on registration
for each row
begin
    select "MAXIME"."REGISTRATION_ID_SEQ".nextval into :new.Id from dual;
end;
/

--School class
create or replace trigger AutoSchoolClass before insert on school_class
for each row
begin
    select "MAXIME"."SCHOOL_CLASS_ID_SEQ".nextval into :new.Id from dual;
end;
/

--School level
create or replace trigger AutoSchoolLevel before insert on school_level
for each row
begin
    select "MAXIME"."SCHOOL_LEVEL_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Section
create or replace trigger AutoSection before insert on section
for each row
begin
    select "MAXIME"."SECTION_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Statement
create or replace trigger AutoStatement before insert on statement
for each row
begin
    select "MAXIME"."STATEMENT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Subject
create or replace trigger AutoSubject before insert on subject
for each row
begin
    select "MAXIME"."SUBJECT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Teaching contract
create or replace trigger AutoTeachingContract before insert on teaching_contract
for each row
begin
    select "MAXIME"."TEACHING_CONTRACT_ID_SEQ".nextval into :new.Id from dual;
end;
/

--Utilisateur
create or replace trigger AutoUtilisateur before insert on utilisateur
for each row
begin
    select "MAXIME"."UTILISATEUR_ID_SEQ".nextval into :new.Id from dual;
end;
/

--lien entre la table utilisateur et le sous table d'utilisateur
create or replace trigger ajout_utilisateur after insert ON UTILISATEUR for each row
begin
    if (:new.TYPE = 'staff') then
        insert into STAFF VALUES (:new.ID);
    end if;
    if (:new.TYPE = 'teacher') then
        insert into TEACHER VALUES (:new.ID);
    end if;
    if (:new.TYPE = 'legalResponsible') then
        insert into LEGAL_RESPONSIBLE VALUES (:new.ID);
    end if;
end;
/

--lien heritage matiere/langue
create or replace trigger ajout_matiere after insert ON SUBJECT for each row
begin
    if (:new.TYPE = 'language') then
        insert into LANGUAGE(ID) VALUES (:new.ID);
    end if;
    if (:new.TYPE = 'optional') then
        insert into OPTIONAL_SUBJECT(ID) VALUES (:new.ID);
    end if;
    if (:new.TYPE = 'commonSubject') then
        insert into COMMON_SUBJECT(ID) VALUES (:new.ID);
    end if;
end;
/
--Fin creation des trigger pour les s�quances

call peuplement_equipment();

--subject
insert into subject(name, disabled, type)VALUES('Fran�ais', 0, 'commonSubject');
insert into subject(name, disabled, type)VALUES('Maths', 0, 'commonSubject');
insert into subject(name, disabled, type)VALUES('Histoire-Geo', 0, 'commonSubject');
insert into subject(name, disabled, type)VALUES('Allemand', 0, 'language');
insert into subject(name, disabled, type)VALUES('Espagnol', 0, 'language');
insert into subject(name, disabled, type)VALUES('E.P.S', 0, 'commonSubject');
insert into subject(name, disabled, type)VALUES('Th��tre', 0, 'optional');


--School Level--
insert into school_level(name, nb_discipline_fac_max)VALUES('Seconde',3);
insert into school_level(name,nb_discipline_fac_max)VALUES('Premi�re',2);
insert into school_level(name,nb_discipline_fac_max)VALUES('Terminale',1);
    
--school_class
call peuplement_school_class(1);

--RH
insert into ADDRESS(STREET_NUMBER,STREET_NAME,POSTALCODE,TOWN) VALUES(
    51,'rue du puit',
    86000, 'Poitiers'
);
insert into utilisateur(address_id, login, roles, password, name, firstname, email,disabled,login_prefix,login_suffix,type) values
            ((Select id from address where street_name = 'rue du puit'),
                'rh1',
                '["ROLE_RH","ROLE_STAFF"]',
                '123',
                'Richard',
                'Honor�',
                'rh@mail.com',
                0,
                'rh',
                1,
                'staff'
            );

--scolarite
call peuplement_scolarite(2);

--documentation
call peuplement_documentation(2);

--teacher
call peuplement_teacher(7);

--student
call peuplement_student(30);

--is_teaches
call peuplement_isteaches();


--Suppression des s�quances
drop trigger AutoAddress;
drop trigger AutoEquipment;
drop trigger AutoEquipmentType;
drop trigger AutoHighSchoolType;
drop trigger AutoIsNoted;
drop trigger AutoIsTeaches;
drop trigger AutoRegistration;
drop trigger AutoSchoolClass;
drop trigger AutoSchoolLevel;
drop trigger AutoSection;
drop trigger AutoStatement;
drop trigger AutoSubject;
drop trigger AutoTeachingContract;
drop trigger AutoUtilisateur;


--Suppression trigger h�ritage
drop trigger ajout_utilisateur;
drop trigger ajout_matiere;

--Fin suppression des s�quances

commit;