create or replace procedure purge_tables as
begin
   -- delete from EQUIPMENT;
    --delete from EQUIPMENT_TYPE;
 --   delete from HIGH_SCHOOL_TYPE;
    delete from REGISTRATION;
    --delete from REGISTRATION_EQUIPMENT;
    delete from LANGUAGE;
    delete from optional_subject;
--    delete from IS_NOTED;
    delete from IS_TAUGHT;
    --delete from IS_TAUGHT_EQUIPMENT;
    delete from SCHOOL_CLASS;
    delete from SCHOOL_LEVEL;
--    delete from SECTION;
--    delete from STATEMENT;
    delete from TEACHING_CONTRACT;
    delete from SUBJECT;
    delete from SCHOOL_CLASS_TEACHING_CONTRACT;
    delete from TEACHER;
    delete from STAFF;
    delete from STUDENT;
    delete from LEGAL_RESPONSIBLE;
    delete from UTILISATEUR;
    delete from ADDRESS;
end;
/