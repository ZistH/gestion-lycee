--Utilisateur
Alter table Utilisateur
rename constraint FK_1D1C63B3F5B7AF75
to FK_Utilisateur_address;

--teachingContract
Alter table Teaching_Contract
rename constraint FK_67E6F86341807E1D
to FK_teaching_contract_teacher;

Alter table Teaching_Contract
rename constraint FK_67E6F86323EDC87
to FK_teaching_contract_subject;

--Teacher
Alter table Teacher
rename constraint FK_B0F6A6D5BF396750
to FK_teacher_Utilisateur;

--Student
Alter table Student
rename constraint FK_B723AF33F4CF9FE6
to FK_STUDENT_LEGAL_RESPONSIBLE_1;

Alter table Student
rename constraint FK_B723AF33E67A3008
to FK_STUDENT_LEGAL_RESPONSIBLE_2;

Alter table Student
rename constraint FK_B723AF33BF396750
to FK_STUDENT_Utilisateur;

--Staff
Alter table Staff
rename constraint FK_426EF392BF396750
to FK_staff_Utilisateur;

--School_class_teaching_contract
Alter table School_class_teaching_contract
rename constraint FK_6D28F10C14463F54
to FK_SCTC_school_class;

Alter table School_class_teaching_contract
rename constraint FK_6D28F10CB9DC9CD6
to FK_SCTC_teaching_contract;

--School_class_teaching_contract
Alter table School_class
rename constraint FK_33B1AF85A1F77FE3
to FK_school_class_school_level;

--registration_equipment
Alter table registration_equipment
rename constraint FK_30642BE6833D8F43
to FK_RE_registration;

Alter table registration_equipment
rename constraint FK_30642BE6517FE9FE
to FK_RE_equipment;

--registration_equipment
Alter table registration
rename constraint FK_62A8A7A7CB944F1A
to FK_REgistration_student;

Alter table registration
rename constraint FK_62A8A7A714463F54
to FK_REgistration_school_class;

Alter table registration
rename constraint FK_62A8A7A782F1BAF4
to FK_REgistration_language;

--OPTIONAL_SUBJECT_REGISTRATION
alter table OPTIONAL_SUBJECT_REGISTRATION
rename constraint FK_A59F55F06DCAD8A6
to FK_OSR_optional_subject;

alter table OPTIONAL_SUBJECT_REGISTRATION
rename constraint FK_A59F55F0833D8F43
to FK_OSR_registration;

--Optional_subject
alter table OPTIONAL_SUBJECT
rename constraint FK_115BC47BBF396750
to FK_OS_subject;

--legal_responsible
alter table legal_responsible
rename constraint FK_82208809BF396750
to FK_LR_Utilisateur;

--language
alter table language
rename constraint FK_D4DB71B5BF396750
to FK_language_subject;

--isteaches_equipment
alter table IS_TAUGHT_equipment
rename constraint FK_C0969E0E24369DDC
to FK_ITE_is_teaches;

alter table IS_TAUGHT_equipment
rename constraint FK_C0969E0E517FE9FE
to FK_ITE_equipment;

--isteaches
alter table IS_TAUGHT
rename constraint FK_D1CD108BA1F77FE3
to FK_IT_school_level;

alter table IS_TAUGHT
rename constraint FK_D1CD108B23EDC87
to FK_IT_subject;

--equipment
alter table equipment
rename constraint FK_D338D583C54C8C93
to FK_equipment_equipment_type;

--common_subject
alter table common_subject
rename constraint FK_7BD06F22BF396750
to FK_CS_subject;

commit;
