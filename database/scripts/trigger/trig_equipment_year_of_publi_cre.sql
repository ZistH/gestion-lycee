--V�rification de la validit� de l'ajout de l'ann�e de publication d'un �quipment
create or replace trigger equipment_year_of_publication before insert ON EQUIPMENT for each row
declare
VTypeEquipment Varchar2(25);
begin
select type into VTypeEquipment
from equipment_type
where id = :new.type_id
;
if VTypeEquipment = 'Manuel' then
            if :new.YEAR_OF_PUBLICATION<2000 or :new.YEAR_OF_PUBLICATION>EXTRACT(YEAR FROM SYSDATE) then
                raise_application_error(-20001, 'Ann�e entr�e non valide');
end if;
end if;
end;
/