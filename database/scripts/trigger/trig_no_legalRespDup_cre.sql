--Pas de doublon de responsable legale dans les utilisateurs
create or replace trigger no_legalRespDup before insert ON UTILISATEUR for each row
declare
    v_LegalResp LEGAL_RESPONSIBLE%rowtype;
    v_Count number;
begin

    if :new.type = 'legalResponsible' then

        Select count(*) into v_Count
            from UTILISATEUR usr
            where :new.type = 'legalResponsible'
            and :new.email = usr.email
        ;
        if v_Count >= 1 then
            Select * into v_LegalResp
                from UTILISATEUR usr
                where :new.type = 'legalResponsible'
                and :new.email = usr.email
            ;

            for row in (
                    select *
                        from LEGAL_RESPONSIBLE_STUDENT lrs
                        where lrs.LEGALRESPONSIBLE_ID = v_LegalResp.ID
                    )
            loop


            end loop;

        end if;
    end if;
end;
/