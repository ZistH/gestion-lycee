create or replace trigger nb_os_for_registration before INSERT ON optional_subject_registration for each row
declare
VNbMaxOptionalSubject number;
    VNbOptionalChose number;
begin
select SL.nb_discipline_fac_max into VNbMaxOptionalSubject
from school_level SL, registration R, school_class SC
where r.id = :new.registration_id
  and sc.id = r.school_class_id
  and sc.school_level_id = sl.id
;

select count(*) into VNbOptionalChose
from optional_subject_registration
where registration_id = :new.registration_id
;

if vnboptionalchose = VNbMaxOptionalSubject then
        raise_application_error(-20001, 'Cet �l�ve a d�j� choisie le nombre maximum d option.');
end if;
end;
/