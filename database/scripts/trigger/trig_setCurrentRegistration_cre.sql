create or replace trigger setCurrentRegistration after INSERT on Registration for each row
DECLARE
   v_student STUDENT%rowtype;
BEGIN
    SELECT * into v_student
        FROM STUDENT
        WHERE id = :new.student_id
    ;

    update STUDENT set CURRENT_REGISTRATION

END;