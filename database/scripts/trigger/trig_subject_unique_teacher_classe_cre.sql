--contrainte : une mati�re est enseign�e par un seul professeur � une classe
create or replace trigger subject_unique_teacher_classe before insert ON SCHOOL_CLASS_Teaching_Contract for each row
declare
VSubjectId number;
    VCount number;
    VYear number;
begin
Select subject_id into VSubjectId
from Teaching_Contract
where id = :new.teaching_contract_id;

SELECT EXTRACT(YEAR FROM SYSDATE) INTO VYear FROM DUAL;

Select count(*) into VCount
from Teaching_Contract TC, SCHOOL_CLASS_Teaching_Contract SCTC
where SCTC.school_class_id = :new.school_class_id
  and tc.year = VYear
  and tc.subject_id = VSubjectId
  and tc.id = sctc.teaching_contract_id;

if(VCount >= 1) then
        raise_application_error(-20001, 'Une mati�re est enseign�e par un SEUL professeur par classe.');
end if;
end;
/
