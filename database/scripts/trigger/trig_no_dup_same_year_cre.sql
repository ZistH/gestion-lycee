--teaching contract no dup for the same year
create or replace trigger no_dup_same_year before INSERT ON teaching_contract for each row
declare
VNbTeachingContract number;
    VNbOptionalChose number;
begin
select count(*) into VNbTeachingContract
from teaching_contract
where teacher_id = :new.teacher_id
          and year = :new.year
;
if VNbTeachingContract >=1 then
        raise_application_error(-20001, 'Ce professeur poss�de d�j� un contract pour l ann�e en cour');
end if;
end;
/