--Suppr�ssion d'un niveau scolaire seulement si rien n'est li�e
create or replace trigger verify_soloLevel before delete ON School_level for each row
declare
VNbSchool_Class number;
    VNbIsTeaches number;
begin
Select count(*) into VNbSchool_Class
from school_class
where school_level_id = :old.id
;
Select count(*) into VNbIsTeaches
from IS_TAUGHT
where school_level_id = :old.id
;
if(VNbSchool_Class >=1 or vnbisteaches >= 1 )then
        raise_application_error(-20001, 'Ce niveau scolaire est li�e � un ou plusieurs autres �l�ment et ne peut donc pas �tre supprim�.');
end if;
end;
/