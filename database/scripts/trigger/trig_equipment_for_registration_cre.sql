--Lien �quipment d'un �tudiant en fonction des mati�re qu'il suit
create or replace trigger equipment_for_registration after insert or update ON IS_TAUGHT_equipment for each row
declare
VLevelId NUMBER;
    VSubjectType Varchar2(50);
    VSubjectId number;
begin

select s.type into VsubjectType
from subject s, IS_TAUGHT it
where s.id = it.subject_id
  and it.id = :new.IS_TAUGHT_id;

select s.id into VSubjectId
from subject s, IS_TAUGHT it
where s.id = it.subject_id
  and it.id = :new.IS_TAUGHT_id;

Select school_level_id into VLevelId
from IS_TAUGHT it
where it.id = :new.IS_TAUGHT_id;

if VsubjectType = 'language' then
        for rec in (select R.id
                    from Registration R, School_class
                    where R.year = EXTRACT(YEAR FROM SYSDATE)
                    and School_class.id = R.school_class_id
                    and school_class.school_level_id = VLevelId
                    and r.language_id = VSubjectId
                ) loop

            insert into registration_equipment values (
                rec.id,
                :new.equipment_id
            );
end loop;

        return;
end if;

    if VsubjectType = 'optional' then
        for rec in (select R.id
                    from Registration R, School_class, Optional_subject_registration OSR
                    where R.year = EXTRACT(YEAR FROM SYSDATE)
                    and School_class.id = R.school_class_id
                    and school_class.school_level_id = VLevelId
                    and osr.registration_id = R.id
                ) loop

            insert into registration_equipment values (
                rec.id,
                :new.equipment_id
            );
end loop;

        return;
end if;

for rec in (select R.id
                    from Registration R, School_class
                    where R.year = EXTRACT(YEAR FROM SYSDATE)
                    and School_class.id = R.school_class_id
                    and school_class.school_level_id = VLevelId
                ) loop

        insert into registration_equipment values (
            rec.id,
            :new.equipment_id
        );
end loop;
end;
/